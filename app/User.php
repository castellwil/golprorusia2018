<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'apellido', 'birthday', 'cedula', 'img', 'telefono', 'email', 'password', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
    
    public function activate()
    {
        $this->update(['active' => true]);
        Auth::login($this);
        $this->token->delete();
    }

    public function token()
    {
        return $this->hasOne(ActivationToken::class);
    }

    public function generateToken()
    {
        $this->token()->create([           
            'token'  => str_random(60)
        ]);
        return $this;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'assigned_roles');
    }

    public function hasRoles(array $roles)
    {
        foreach($roles as $role)
        {
            foreach($this->roles as $userRole)
            {
                if($userRole->name === $role)
                {
                    return true;
                }
            }              
        }
        return false;
    }

    public function isAdmin()
    {
        return $this->hasRoles(['admin']);
    }
    public function point()
    {
        return $this->belongsTo(Point::class);
    }

    public function favoritas()
    {
        return $this->belongsToMany(Favorito::class);
    }

}

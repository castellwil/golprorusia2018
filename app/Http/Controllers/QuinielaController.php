<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiniela;
use App\Seleccion;
use App\Http\Requests\FillQuinielaRequest;
use App\GrupoA;
use App\GrupoB;
use App\GrupoC;
use App\GrupoD;
use App\GrupoE;
use App\GrupoF;
use App\GrupoG;
use App\GrupoH;
use App\User;
use App\Role;
use Redirect;
use Carbon\Carbon;
use DB;
use App\Http\Middleware\CheckRoles;
Use Auth;

class QuinielaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth',['except'=>['configQuinielas', 'index', 'comprobarTabla']]);
        $this->middleware('roles:admin',['except'=>['configQuinielas', 'index']]);
    }     

     public function index($id=null)

     {         
        
        if($id == null) { $id = \Auth::user()->id; } 
        $partidosA = GrupoA::where('user_id','=',$id)->get();         
        $partidosB = GrupoB::where('user_id','=',$id)->get();         
        $partidosC = GrupoC::where('user_id','=',$id)->get();         
        $partidosD = GrupoD::where('user_id','=',$id)->get();         
        $partidosE = GrupoE::where('user_id','=',$id)->get();         
        $partidosF = GrupoF::where('user_id','=',$id)->get();         
        $partidosG = GrupoG::where('user_id','=',$id)->get();         
        $partidosH = GrupoH::where('user_id','=',$id)->get(); 
        
        return view('quiniela.index', compact('partidosA','partidosB','partidosC','partidosD','partidosE','partidosF','partidosG','partidosH'));

     }   


    public function show($id)
    {       
    } 

    public function comprobarTabla($user)
    {      
        $user = \Auth::user()->id; 
        $partidos = GrupoA::where('user_id','=',$user)->get();               
        if(count($partidos)> 0){
            return redirect('/quiniela')->with('info', 'Ya tienes la quiniela lista para llenar');
        }        
    }
    
    public function configApp()
    {
        // $this->crearParDeUsers();
        $this->crearTablaSelecciones();
        $this->crearParDeRoles();
        return redirect('/');
    }

    public function crearTablaSelecciones()
    {
        $code =[
            'A1', 'A2', 'A3', 'A4', 'B1', 'B2', 'B3', 'B4', 'C1', 'C2', 'C3', 'C4', 'D1', 'D2', 'D3', 'D4', 'E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4', 'G1', 'G2', 'G3', 'G4', 'H1', 'H2', 'H3', 'H4'            
        ]; 
        $pais =[
            'Rusia', 'Arabia Saudita', 'Egipto', 'Uruguay', 'Portugal', 'España', 'Marruecos', 'Irán', 'Francia', 'Australia', 'Perú', 'Dinamarca', 'Argentina', 'Islandia', 'Croacia', 'Nigeria', 'Brasil', 'Suiza', 'Costa Rica', 'Serbia', 'Alemania', 'México', 'Suecia', 'Corea', 'Bélgica', 'Panamá', 'Tunez', 'Inglaterra', 'Polonia', 'Senegal', 'Colombia', 'Japón'         
        ];         
        $bandera =[
            'Rus', 'Ara', 'Egi', 'Uru', 'Por', 'Esp', 'Mar', 'Ira', 'Fra', 'Aus', 'Per', 'Din', 'Arg', 'Isl', 'Cro', 'Nig', 'Bra', 'Sui', 'Cost', 'Ser', 'Ale', 'Mex', 'Sue', 'Cor', 'Bel', 'Pan', 'Tun', 'Ing', 'Pol', 'Sen', 'Col', 'Jap'         
        ];         

        for ($i=0; $i < 32 ; $i++) { 
            $seleccion = new Seleccion;
            $seleccion->code =  $code[$i];
            $seleccion->pais = $pais[$i];           
            $seleccion->bandera = $bandera[$i];           
            $seleccion->save();
        }    
    }

    public function crearParDeUsers()
    {       
        $name = ['Admin', 'Wilmer']; 
        $lastname = ['Golpro', 'Castellanos']; 
        $cum = ['1968-12-23', '1980-10-19']; 
        $telf = ['123456789', '04147585086'];  
        $ced = ['123456789', '14605145'];  
        $mail = ['systemprogrampro@gmail.com', 'wilmerjaviercastellanos@gmail.com'];  
        $imagen = ['/img/player.jpg', '/img/player.jpg'];  
                   
        for ($i=0; $i < 2 ; $i++) { 
            $usuario = new User;
            $usuario->nombre = $name[$i]; 
            $usuario->apellido = $lastname[$i]; 
            $usuario->birthday = $cum[$i]; 
            $usuario->telefono = $telf[$i];  
            $usuario->cedula = $ced[$i];  
            $usuario->email = $mail[$i];  
            $usuario->img = $imagen[$i];          
            $usuario->password = bcrypt('LIsumubi24');           
            $usuario->save();
        }
    }

    public function crearParDeRoles()
    {       
        $nombre = ['admin', 'player', 'vip']; 
        $mostrar = ['Admin', 'Player','Vip']; 
        $desc = ['Administrador', 'Jugador', 'Jugador Vip'];        

        for ($i=0; $i < 3 ; $i++) { 
            $role = new Role;
            $role->name = $nombre[$i]; 
            $role->display_name = $mostrar[$i]; 
            $role->description = $desc[$i];                     
            $role->save();
        }
    } 

    public function configQuinielas()
    {
        $error = null;
        $user = \Auth::user()->id;
        if(!$this->comprobarTabla($user) )
        {
                DB::beginTransaction();
                try {
                        $this->createGrupoA();
                        $this->createGrupoB();
                        $this->createGrupoC();
                        $this->createGrupoD();
                        $this->createGrupoE();
                        $this->createGrupoF();
                        $this->createGrupoG();
                        $this->createGrupoH();
                    DB::commit();
                    $success = true;      
                    return redirect('/grupoA')->with('info', 'Ya tienes la quiniela lista para llenar');
                    } catch (\Exception $e) {
                        $success = false;
                        $error = $e->getMessage();
                        DB::rollback();
                    } 
                }          
        else {
            return redirect('/quiniela')->with('info', 'Ya tienes la quiniela lista para llenar');
        }       
    }
    
    public function createGrupoA()
    {     
        $equipo = Seleccion::all();
        $A1 = $equipo[0]['pais']; $cA1 = $equipo[0]['code'];
        $A2 = $equipo[1]['pais']; $cA2 = $equipo[1]['code'];
        $A3 = $equipo[2]['pais']; $cA3 = $equipo[2]['code'];
        $A4 = $equipo[3]['pais']; $cA4 = $equipo[3]['code'];

        $bA1 = $equipo[0]['bandera'];
        $bA2 = $equipo[1]['bandera'];
        $bA3 = $equipo[2]['bandera'];
        $bA4 = $equipo[3]['bandera'];

        $code = [$cA1.$cA2,$cA3.$cA4, $cA1.$cA3, $cA4.$cA2,$cA2.$cA3, $cA4.$cA1];       
        $equipo_A = [$A1, $A3, $A1, $A4, $A2, $A4];
        $flat_A =   [$bA1, $bA3, $bA1, $bA4, $bA2, $bA4];
        $equipo_B = [$A2, $A4, $A3, $A2, $A3, $A1];
        $flat_B = [$bA2, $bA4, $bA3, $bA2, $bA3, $bA1];

        $grupoAActive = [
            '1',   //  A1A2
            '1',   //  A3A4
            '0',   //  A1A3
            '0',   //  A4A2
            '0',   //  A2A3
            '0',   //  A4A1
        ];
        
        $hourGame =['2018-06-14 11:00:00', '2018-06-15 08:00:00', '2018-06-19 14:00:00', '2018-06-20 11:00:00', '2018-06-25 10:00:00', '2018-06-25 10:00:00'];       

        for ($i=0; $i < 6 ; $i++) { 
            $cuenta = new GrupoA;
            $cuenta->code =  $code[$i];
            $cuenta->grupoFase = 'Grupo A';
            $cuenta->equipo_A = $equipo_A[$i];
            $cuenta->flat_A = $flat_A[$i];
            $cuenta->equipo_B = $equipo_B[$i];            
            $cuenta->flat_B = $flat_B[$i];            
            $cuenta->activeGame = $grupoAActive[$i];
            $cuenta->hourGame = $hourGame[$i];
            $cuenta->visible = 1;
            $cuenta->user_id =  \Auth::user()->id; 
            $cuenta->save();
        }       
    }
    
    public function createGrupoB()
    {
        $equipo = Seleccion::all();
        $A1 = $equipo[4]['pais']; $cA1 = $equipo[4]['code'];
        $A2 = $equipo[5]['pais']; $cA2 = $equipo[5]['code'];
        $A3 = $equipo[6]['pais']; $cA3 = $equipo[6]['code'];
        $A4 = $equipo[7]['pais']; $cA4 = $equipo[7]['code'];

        $bA1 = $equipo[4]['bandera'];
        $bA2 = $equipo[5]['bandera'];
        $bA3 = $equipo[6]['bandera'];
        $bA4 = $equipo[7]['bandera'];

        $code = [$cA1.$cA2,$cA3.$cA4, $cA1.$cA3, $cA4.$cA2,$cA2.$cA3, $cA4.$cA1];       
        $equipo_A = [$A1, $A3, $A1, $A4, $A2, $A4];
        $flat_A = [$bA1, $bA3, $bA1, $bA4, $bA2, $bA4];
        $equipo_B = [$A2, $A4, $A3, $A2, $A3, $A1]; 
        $flat_B = [$bA2, $bA4, $bA3, $bA2, $bA3, $bA1];  

        $grupoBActive = [
            '1',   //  B1B2
            '1',   //  B3B4
            '0',   //  B1B3
            '0',   //  B4B2
            '0',   //  B2B3
            '0'   //  B4B1
        ];
        
        $hourGame =['2018-06-15 14:00:00', '2018-06-15 11:00:00', '2018-06-20 08:00:00', '2018-06-20 11:00:00', '2018-06-20 14:00:00',  '2018-06-25 14:00:00'];  

        for ($i=0; $i < 6 ; $i++) { 
            $cuenta = new GrupoB;
            $cuenta->code =  $code[$i];
            $cuenta->grupoFase = 'Grupo B';
            $cuenta->equipo_A = $equipo_A[$i];
            $cuenta->flat_A = $flat_A[$i];
            $cuenta->equipo_B = $equipo_B[$i];            
            $cuenta->flat_B = $flat_B[$i];            
            $cuenta->activeGame = $grupoBActive[$i];
            $cuenta->hourGame = $hourGame[$i];
            $cuenta->visible = 1;
            $cuenta->user_id =  \Auth::user()->id; 
            $cuenta->save();
        }        
    }

    public function createGrupoC()
    {
        $equipo = Seleccion::all();
        $A1 = $equipo[8]['pais']; $cA1 = $equipo[8]['code'];
        $A2 = $equipo[9]['pais']; $cA2 = $equipo[9]['code'];
        $A3 = $equipo[10]['pais']; $cA3 = $equipo[10]['code'];
        $A4 = $equipo[11]['pais']; $cA4 = $equipo[11]['code'];

        $bA1 = $equipo[8]['bandera'];
        $bA2 = $equipo[9]['bandera'];
        $bA3 = $equipo[10]['bandera'];
        $bA4 = $equipo[11]['bandera'];

        $grupoCActive = [
            '1',   //  C1C2
            '1',   //  C3C4
            '0',   //  C1C3
            '0',   //  C4C2
            '0',   //  C2C3
            '0'   //  C4C1
        ];

        $hourGame =[
            '2018-06-16 06:00:00', //A1A2
            '2018-06-16 12:00:00', //A3A4
            '2018-06-21 11:00:00', //A1A23
            '2018-06-21 08:00:00', //A4A2
            '2018-06-26 10:00:00', //A2A3
            '2018-06-26 10:00:00' //A4A1
        ];  

        $code = [$cA1.$cA2,$cA3.$cA4, $cA1.$cA3, $cA4.$cA2,$cA2.$cA3, $cA4.$cA1];       
        $equipo_A = [$A1, $A3, $A1, $A4, $A2, $A4];
        $flat_A =   [$bA1, $bA3, $bA1, $bA4, $bA2, $bA4];
        $equipo_B = [$A2, $A4, $A3, $A2, $A3, $A1];
        $flat_B = [$bA2, $bA4, $bA3, $bA2, $bA3, $bA1];  

        for ($i=0; $i < 6 ; $i++) { 
            $cuenta = new GrupoC;
            $cuenta->code =  $code[$i];
            $cuenta->grupoFase = 'Grupo C';
            $cuenta->equipo_A = $equipo_A[$i];
            $cuenta->flat_A = $flat_A[$i];
            $cuenta->equipo_B = $equipo_B[$i];            
            $cuenta->flat_B = $flat_B[$i];            
            $cuenta->activeGame = $grupoCActive[$i];
            $cuenta->hourGame = $hourGame[$i];
            $cuenta->visible = 1;
            $cuenta->user_id =  \Auth::user()->id; 
            $cuenta->save();
        }        
    }

    public function createGrupoD()
    {
        $equipo = Seleccion::all();
        $A1 = $equipo[12]['pais']; $cA1 = $equipo[12]['code'];
        $A2 = $equipo[13]['pais']; $cA2 = $equipo[13]['code'];
        $A3 = $equipo[14]['pais']; $cA3 = $equipo[14]['code'];
        $A4 = $equipo[15]['pais']; $cA4 = $equipo[15]['code'];

        $bA1 = $equipo[12]['bandera'];
        $bA2 = $equipo[13]['bandera'];
        $bA3 = $equipo[14]['bandera'];
        $bA4 = $equipo[15]['bandera'];

        $grupoDActive = [
            '1',   //  D1D2
            '0',   //  D3D4
            '0',   //  D1D3
            '0',   //  D4D2
            '0',   //  D2D3
            '0'   //  D4D1
        ];

        $hourGame =[
            '2018-06-16 09:00:00', //A1A2
            '2018-06-16 15:00:00', //A3A4
            '2018-06-21 14:00:00', //A1A23
            '2018-06-22 11:00:00', //A4A2
            '2018-06-26 14:00:00', //A2A3
            '2018-06-26 14:00:00' //A4A1
        ];  

        $code = [$cA1.$cA2,$cA3.$cA4, $cA1.$cA3, $cA4.$cA2,$cA2.$cA3, $cA4.$cA1];       
        $equipo_A = [$A1, $A3, $A1, $A4, $A2, $A4];
        $flat_A =   [$bA1, $bA3, $bA1, $bA4, $bA2, $bA4];
        $equipo_B = [$A2, $A4, $A3, $A2, $A3, $A1];
        $flat_B = [$bA2, $bA4, $bA3, $bA2, $bA3, $bA1];      

        for ($i=0; $i < 6 ; $i++) { 
            $cuenta = new GrupoD;
            $cuenta->code =  $code[$i];
            $cuenta->grupoFase = 'Grupo D';
            $cuenta->equipo_A = $equipo_A[$i];
            $cuenta->flat_A = $flat_A[$i];
            $cuenta->equipo_B = $equipo_B[$i];            
            $cuenta->flat_B = $flat_B[$i];            
            $cuenta->activeGame = $grupoDActive[$i];
            $cuenta->hourGame = $hourGame[$i];
            $cuenta->visible = 1;
            $cuenta->user_id =  \Auth::user()->id; 
            $cuenta->save();
        }        
    }

    public function createGrupoE()
    {
        $equipo = Seleccion::all();
        $A1 = $equipo[16]['pais']; $cA1 = $equipo[16]['code'];
        $A2 = $equipo[17]['pais']; $cA2 = $equipo[17]['code'];
        $A3 = $equipo[18]['pais']; $cA3 = $equipo[18]['code'];
        $A4 = $equipo[19]['pais']; $cA4 = $equipo[19]['code'];

        
        $bA1 = $equipo[16]['bandera'];
        $bA2 = $equipo[17]['bandera'];
        $bA3 = $equipo[18]['bandera'];
        $bA4 = $equipo[19]['bandera'];

        $code = [$cA1.$cA2,$cA3.$cA4, $cA1.$cA3, $cA4.$cA2,$cA2.$cA3, $cA4.$cA1];       
        $equipo_A = [$A1, $A3, $A1, $A4, $A2, $A4];
        $flat_A =   [$bA1, $bA3, $bA1, $bA4, $bA2, $bA4];
        $equipo_B = [$A2, $A4, $A3, $A2, $A3, $A1];
        $flat_B = [$bA2, $bA4, $bA3, $bA2, $bA3, $bA1];

        $grupoEActive = [
            '0',   //  E1E2
            '0',   //  E3E4
            '0',   //  E1E3
            '0',   //  E4E2
            '0',   //  E2E3
            '0'   //  E4E1
        ];

        $hourGame =[
            '2018-06-17 14:00:00', //A1A2
            '2018-06-17 08:00:00', //A3A4
            '2018-06-22 08:00:00', //A1A23
            '2018-06-22 14:00:00', //A4A2
            '2018-06-27 14:00:00', //A2A3
            '2018-06-27 14:00:00' //A4A1
        ];        

        for ($i=0; $i < 6 ; $i++) { 
            $cuenta = new GrupoE;
            $cuenta->code =  $code[$i];
            $cuenta->grupoFase = 'Grupo E';
          $cuenta->equipo_A = $equipo_A[$i];
            $cuenta->flat_A = $flat_A[$i];
            $cuenta->equipo_B = $equipo_B[$i];            
            $cuenta->flat_B = $flat_B[$i];            
            $cuenta->activeGame = $grupoEActive[$i];
            $cuenta->hourGame = $hourGame[$i];
            $cuenta->visible = 1;
            $cuenta->user_id =  \Auth::user()->id; 
            $cuenta->save();
        }        
    }

    public function createGrupoF()
    {
        $equipo = Seleccion::all();
        $A1 = $equipo[20]['pais']; $cA1 = $equipo[20]['code'];
        $A2 = $equipo[21]['pais']; $cA2 = $equipo[21]['code'];
        $A3 = $equipo[22]['pais']; $cA3 = $equipo[22]['code'];
        $A4 = $equipo[23]['pais']; $cA4 = $equipo[23]['code'];

        $bA1 = $equipo[20]['bandera'];
        $bA2 = $equipo[21]['bandera'];
        $bA3 = $equipo[22]['bandera'];
        $bA4 = $equipo[23]['bandera'];

        $grupoFActive = [
            '0',   //  F1F2
            '0',   //  F3F4
            '0',   //  F1F3
            '0',   //  F4F2
            '0',   //  F2F3
            '0'   //  F4F1
        ];

        $hourGame =[
            '2018-06-17 11:00:00', //A1A2
            '2018-06-18 08:00:00', //A3A4
            '2018-06-23 14:00:00', //A1A23
            '2018-06-23 11:00:00', //A4A2
            '2018-06-27 10:00:00', //A2A3
            '2018-06-27 10:00:00' //A4A1
        ];

        $code = [$cA1.$cA2,$cA3.$cA4, $cA1.$cA3, $cA4.$cA2,$cA2.$cA3, $cA4.$cA1];       
        $equipo_A = [$A1, $A3, $A1, $A4, $A2, $A4];
        $flat_A =   [$bA1, $bA3, $bA1, $bA4, $bA2, $bA4];
        $equipo_B = [$A2, $A4, $A3, $A2, $A3, $A1];
        $flat_B = [$bA2, $bA4, $bA3, $bA2, $bA3, $bA1];      

        for ($i=0; $i < 6 ; $i++) { 
            $cuenta = new GrupoF;
            $cuenta->code =  $code[$i];
            $cuenta->grupoFase = 'Grupo F';
            $cuenta->equipo_A = $equipo_A[$i];
            $cuenta->flat_A = $flat_A[$i];
            $cuenta->equipo_B = $equipo_B[$i];            
            $cuenta->flat_B = $flat_B[$i];            
            $cuenta->activeGame = $grupoFActive[$i];
            $cuenta->hourGame = $hourGame[$i];
            $cuenta->visible = 1;
            $cuenta->user_id =  \Auth::user()->id; 
            $cuenta->save();
        }       
    }

    public function createGrupoG()
    {
        $equipo = Seleccion::all();
        $A1 = $equipo[24]['pais']; $cA1 = $equipo[24]['code'];
        $A2 = $equipo[25]['pais']; $cA2 = $equipo[25]['code'];
        $A3 = $equipo[26]['pais']; $cA3 = $equipo[26]['code'];
        $A4 = $equipo[27]['pais']; $cA4 = $equipo[27]['code'];

        $bA1 = $equipo[24]['bandera'];
        $bA2 = $equipo[25]['bandera'];
        $bA3 = $equipo[26]['bandera'];
        $bA4 = $equipo[27]['bandera'];

        $grupoGActive = [
            '0',   //  G1G2
            '0',   //  G3G4
            '0',   //  G1G3
            '0',   //  G4G2
            '0',   //  G2G3
            '0'   //  G4G1
        ];

        $hourGame =[
            '2018-06-18 11:00:00', //A1A2
            '2018-06-18 14:00:00', //A3A4
            '2018-06-23 08:00:00', //A1A23
            '2018-06-24 08:00:00', //A4A2
            '2018-06-28 14:00:00', //A2A3
            '2018-06-28 14:00:00' //A4A1
        ];
        $code = [$cA1.$cA2,$cA3.$cA4, $cA1.$cA3, $cA4.$cA2,$cA2.$cA3, $cA4.$cA1];       
        $equipo_A = [$A1, $A3, $A1, $A4, $A2, $A4];
        $flat_A =   [$bA1, $bA3, $bA1, $bA4, $bA2, $bA4];
        $equipo_B = [$A2, $A4, $A3, $A2, $A3, $A1];
        $flat_B = [$bA2, $bA4, $bA3, $bA2, $bA3, $bA1];       

        for ($i=0; $i < 6 ; $i++) { 
            $cuenta = new GrupoG;
            $cuenta->code =  $code[$i];
            $cuenta->grupoFase = 'Grupo G';
            $cuenta->equipo_A = $equipo_A[$i];
            $cuenta->flat_A = $flat_A[$i];
            $cuenta->equipo_B = $equipo_B[$i];            
            $cuenta->flat_B = $flat_B[$i];            
            $cuenta->activeGame = $grupoGActive[$i];
            $cuenta->hourGame = $hourGame[$i];
            $cuenta->visible = 1;
            $cuenta->user_id =  \Auth::user()->id; 
            $cuenta->save();
        }        
    }

    public function createGrupoH()
    {
        $equipo = Seleccion::all();
        $A1 = $equipo[28]['pais']; $cA1 = $equipo[28]['code'];
        $A2 = $equipo[29]['pais']; $cA2 = $equipo[29]['code'];
        $A3 = $equipo[30]['pais']; $cA3 = $equipo[30]['code'];
        $A4 = $equipo[31]['pais']; $cA4 = $equipo[31]['code'];

        $bA1 = $equipo[28]['bandera'];
        $bA2 = $equipo[29]['bandera'];
        $bA3 = $equipo[30]['bandera'];
        $bA4 = $equipo[31]['bandera'];

        $grupoHActive = [
            '0',   //  H1H2
            '0',   //  H3H4
            '0',   //  H1H3
            '0',   //  H4H2
            '0',   //  H2H3
            '1'   //  H4H1
        ];

        $hourGame =[
            '2018-06-19 11:00:00', //A1A2
            '2018-06-19 08:00:00', //A3A4
            '2018-06-24 14:00:00', //A1A23
            '2018-06-24 11:00:00', //A4A2
            '2018-06-28 10:00:00', //A2A3
            '2018-06-28 10:00:00' //A4A1
        ];

        $code = [$cA1.$cA2,$cA3.$cA4, $cA1.$cA3, $cA4.$cA2,$cA2.$cA3, $cA4.$cA1];       
        $equipo_A = [$A1, $A3, $A1, $A4, $A2, $A4];
        $flat_A =   [$bA1, $bA3, $bA1, $bA4, $bA2, $bA4];
        $equipo_B = [$A2, $A4, $A3, $A2, $A3, $A1];
        $flat_B = [$bA2, $bA4, $bA3, $bA2, $bA3, $bA1];    

        for ($i=0; $i < 6 ; $i++) { 
            $cuenta = new GrupoH;
            $cuenta->code =  $code[$i];
            $cuenta->grupoFase = 'Grupo H';
            $cuenta->equipo_A = $equipo_A[$i];
            $cuenta->flat_A = $flat_A[$i];
            $cuenta->equipo_B = $equipo_B[$i];            
            $cuenta->flat_B = $flat_B[$i];            
            $cuenta->activeGame = $grupoHActive[$i];
            $cuenta->hourGame = $hourGame[$i];
            $cuenta->visible = 1;
            $cuenta->user_id =  \Auth::user()->id; 
            $cuenta->save();
        }        
    }

     public function CerrarPartido($a,$dos,$tres)
     {
        //$a sera el grupo ['grupo_a'.'grupo_b', 'grupo_c']
        //$dos será el código del partido A1A2 A3A4 A1A3 A4A2 A2A3 A4A1... B1B2 B3B4
        //$tres 1 cerrado 0 abierto    

            $sql = "UPDATE $a SET activeGame= $tres WHERE  code = '$dos'";            
            DB::update($sql);
            return "hecho";  
    }         
    
}

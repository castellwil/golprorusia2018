<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Point;
use App\Favorito;
use DB;

class ListaUsuariosController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('participantes.index', compact('users'));
    }

    // public function isadminRole()
    //  {                                    
    //         $sql = "INSERT INTO assigned_roles (user_id, role_id) VALUES (1, 1)";            
    //         DB::update($sql);
    //         return "hecho";  
          
    //  }  
    public function favoritas()
    {
               
        $user = 1; 
        $quinielas_favoritas = Favorito::where('user_id','=',$user)->get()->toArray();         
        $users = User::where('id','=', $quinielas_favoritas)->get(); 
        
        return view('participantes.favoritas', compact('users'));  
       
        
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GrupoE;
use App\Http\Requests\FillQuinielaRequest;
use Redirect;

class GrupoEController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['privacity', 'terms']]);
        
    }
    
    public function index()
    {
        $user = \Auth::user()->id;         
        $partidos = GrupoE::where('user_id','=',$user)->get();        
        return view('quiniela.grupo_e.index', compact('partidos'));
    }

    public function create()
    {       
    }

    public function store()
    {       
    }

    public function edit($id)
    {                                  
        $user = \Auth::user()->id;         
        $matches = GrupoE::where('user_id','=',$user)->get();
        $partido = GrupoE::findOrFail($id);          
        return view('quiniela.grupo_e.edit', compact('partido','matches'));
    }


    public function update(FillQuinielaRequest $request,  $id)
    {
        $partido = GrupoE::find($id);
       
        if(!$partido->activeGame == 1){
            $data = $request->all();
            $user_id = $data["user_id"];
            $partido->goles_A = $data["goles_A"];
            $partido->goles_B = $data["goles_B"];
            $partido->minGolesA = $data["minGolesA"];
            $partido->minGolesA = $data["minGolesA"];
            $partido->minGolesB = $data["minGolesB"];        

            $resul = $partido->save();
                if ($resul) {
                    return back()->with('info', 'Usuario Actualizado');
                } else {
                    return view("mensajes.rechazado")->with("msj", "hubo un error vuelva a intentarlo");
                }
        } else {
            return back()->with('info-err', 'Partido está cerrado, No se puede actualizar');
        }
    }

    public function show()
    {       
    }
}

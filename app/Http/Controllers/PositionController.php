<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Point;
use App\User;
use App\Position;
use DB;


class PositionController extends Controller
{
    public function index()
    {
            $points = Position::orderBy('puntos','desc')->get();
           $key = 1;
        
        return view('participantes.tabla', compact('points','key'));
    }
    
    public static function basePosition()
    {
        $users = User::all();
        $cantidadUsers = count($users); 
       
        $inicio = 1;
        $final = $cantidadUsers;
                            
        for($i = $inicio; $i <= $final; $i++)
            {     
                $position = new Position;                
                $position->aciertos_parciales = 0;
                $position->aciertos_totales = 0;
                $position->puntos = 0;
                $position->user_id = $i;                
                $position->save();
            }  
            return "Bien";
        }  

}

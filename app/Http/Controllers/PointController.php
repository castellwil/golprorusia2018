<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Point;
use App\User;
use App\GrupoA;
use App\GrupoB;
use App\GrupoC;
use App\GrupoD;
use App\GrupoE;
use App\GrupoF;
use App\GrupoG;
use App\Position;
use App\GrupoH;
use App\Quiniela;
use DB;

class PointController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    public function index()
    {
        $points = Point::orderBy('puntos','desc')->orderBy('aciertos_totales','desc')->orderBy('goles','desc')->get();
        return view('participantes.tabla', compact('points'));
    }    

    public function procesarPartidoyCargarPuntos($code)
    { 
        $partido1 =  $this->OldMatch($code);

        if(!$partido1){
            $partido1 = $this->ProcesadorMatch($code);           
          } else {
            return " Partido ya procesado";
        }
        
    }

    public function OldMatch($code)
    {
        $codeProcesado = Point::where('username','=',$code)->get()->toArray();
        $verificar =  Count($codeProcesado);       
        return $verificar;
      
    }

    public function normalizarDatoMingol($ga)
    {
        if($ga === 0  || is_null($ga) ||empty($ga) )
        {
           $ga = [];
        } 
        return $ga;
    }    

    public function obtenerPuntaje($a, $b, $A,  $B)
    {
        $gep = $this->ganaEmpataPierde($a, $b, $A,  $B);
        $gs = $this->verificarGoles($a, $A);
        $gi = $this->verificarGoles($b, $B);
        $puntaje = $gep+$gs+$gi;
        $AT = 0;
        $AP = 0;
        if($puntaje == 5) {
            $AT++;
        } 
        elseif($puntaje > 1 && $puntaje <= 4) {
            $AP++;
        } else {
            $AP;
        }
        return array($puntaje,$AT, $AP);
    }

    public function ganaEmpataPierde($a, $b, $A, $B)
    {           
        if(is_null($a)  || is_null($b))
        {
            $pronostico = 0;
            return $pronostico;      
        } 
        
        if($a > $b && $A > $B)
        {
            $pronostico = 3;
            return $pronostico;      
        }         

        if($a < $b && $A < $B)
        {
            $pronostico = 3;           
            return $pronostico;
        } 

        if ($a == $b && $A == $B)
        {
            $pronostico = 3;             
            return $pronostico;
        }
        return $pronostico = 0;
    }

    public function verificarGoles($a, $b)
    {
        if(is_null($a) || is_null($b))
        {
            $a = 999;
            $b = 998;
        }    
        elseif ($a ==  $b )
        {
            $goles = 1;             
            return $goles;
        }
        return $goles = 0;         
    }

    public function datosPartidoAProcesar($code)
    {
        if($code == 'A1A2' ||  $code == 'A3A4' ||  $code == 'A1A3'||  $code == 'A4A2'|| $code == 'A2A3'|| $code == 'A4A1')
        {
            return $partido = GrupoA::where('code', '=', $code)->get()->toArray();
        }
       elseif($code == 'B1B2' ||  $code == 'B3B4' ||  $code == 'B1B3'||  $code == 'B4B2'|| $code == 'B2B3'|| $code == 'B4B1')
        {
            return $partido = GrupoB::where('code', '=', $code)->get()->toArray();
        }
    
        elseif($code == 'C1C2' ||  $code == 'C3C4' ||  $code == 'C1C3'||  $code == 'C4C2'|| $code == 'C2C3'|| $code == 'C4C1')
        {
            return $partido = GrupoC::where('code', '=', $code)->get()->toArray();
        }
        
        elseif($code == 'D1D2' ||  $code == 'D3D4' ||  $code == 'D1D3'||  $code == 'D4D2'|| $code == 'D2D3'|| $code == 'D4D1')
        {
            return $partido = GrupoD::where('code', '=', $code)->get()->toArray();
        }

        elseif($code == 'E1E2' ||  $code == 'E3E4' ||  $code == 'E1E3'||  $code == 'E4E2'|| $code == 'E2E3'|| $code == 'E4E1')
        {
            return $partido = GrupoE::where('code', '=', $code)->get()->toArray();
        }

        elseif($code == 'F1F2' ||  $code == 'F3F4' ||  $code == 'F1F3'||  $code == 'F4F2'|| $code == 'F2F3'|| $code == 'F4F1')
        {
            return $partido = GrupoF::where('code', '=', $code)->get()->toArray();
        }

        elseif($code == 'G1G2' ||  $code == 'G3G4' ||  $code == 'G1G3'||  $code == 'G4G2'|| $code == 'G2G3'|| $code == 'G4G1')
        {
            return $partido = GrupoG::where('code', '=', $code)->get()->toArray();
        }

        elseif($code == 'H1H2' ||  $code == 'H3H4' ||  $code == 'H1H3'||  $code == 'H4H2'|| $code == 'H2H3'|| $code == 'H4H1')
        {
            return $partido = GrupoH::where('code', '=', $code)->get()->toArray();
        }      
       
        else {
            return $partido = "Partido No registrado";
        }        
    } 
    public function ResultadoOficialMatch($code)
    {        
        return $partido = Quiniela::where('code', '=', $code)->get()->toArray();        
    }

    public function positionPrevia()
    {
        $positions = Position::get()->toArray();
        return $positions;
    }

    public function ProcesadorMatch($code)
    {
        $partidosARevisar = $this->datosPartidoAProcesar($code);
        $partidoOficialARevisar = $this->ResultadoOficialMatch($code);
        $cantidadpartidos = count($partidosARevisar);        
        
        //Datos Partido Oficial
        $GOLA = $partidoOficialARevisar[0]['goles_A'];
        $GOLB = $partidoOficialARevisar[0]['goles_B']; 
        $MINGOLA = $partidoOficialARevisar[0]['minGolesA']; 
        $MINGOLB = $partidoOficialARevisar[0]['minGolesB']; 
        //$PuntosPrevios = $this->positionPrevia();            
            
           for($i = 0; $i < $cantidadpartidos; $i++)
            {
                $user = $partidosARevisar[$i]['user_id'];
                $gola = $partidosARevisar[$i]['goles_A'];
                $golb = $partidosARevisar[$i]['goles_B'];
                $partidoQui = $this->obtenerPuntaje($gola,$golb, $GOLA,  $GOLB);  
                $total = $partidoQui[0];   
                $AT = $partidoQui[1];   
                $AP = $partidoQui[2];                                     

                $puntosPartidos = new Point;                
                $puntosPartidos->aciertos_parciales = $AP;
                $puntosPartidos->aciertos_totales = $AT;
                $puntosPartidos->puntos = $total;
                $puntosPartidos->user_id = $user;
                $puntosPartidos->username = $code;
                $puntosPartidos->save();

                // $positionPreviaAp = $PuntosPrevios[$i]['aciertos_parciales'] + $AP;
                // $positionPreviaAt = $PuntosPrevios[$i]['aciertos_totales'] + $AT;
                // $positionPreviaAt = $PuntosPrevios[$i]['goles'] + $goles;
                // $positionPreviaTotal = $PuntosPrevios[$i]['puntos'] + $total;         
        
                // $sql = "UPDATE positions 
                //         SET aciertos_parciales = $positionPreviaAp,
                //             aciertos_totales = $positionPreviaAt,
                //             puntos = $positionPreviaTotal,
                //             username = '$code'
                //         WHERE  user_id = $user";            
                // DB::update($sql);      
            }  
            return "Partidos Procesados";
    }

    public function renovarLista()
    {
        $sql = "DROP TABLE IF EXISTS positions;
                    CREATE TABLE positions AS
                    select user_id, sum(puntos) as puntos
                    from points
                    group by user_id
                    order by puntos Desc";
         DB::update($sql); 
    }

    public function ActualizarPuntosPartido($code)
    {
        $error = null;        
        if(true )
        {
                DB::beginTransaction();
                try {
                        $this->ProcesadorMatch($code);                       
                        $this->renovarLista();                       
                    DB::commit();
                    $success = true;      
                    return redirect('/tabla')->with('info', 'Ya se procesó los datos de la tabla');
                    } catch (\Exception $e) {
                        $success = false;
                        $error = $e->getMessage();
                        DB::rollback();
                    } 
                }          
        else {
            return redirect('/quiniela')->with('Falló');
        }       
    }   

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GrupoH;
use App\Http\Requests\FillQuinielaRequest;
use Redirect;

class GrupoHController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['privacity', 'terms']]);
        
    }
    public function index()
    {
        $user = \Auth::user()->id;         
        $partidos = GrupoH::where('user_id','=',$user)->get();        
        return view('quiniela.grupo_h.index', compact('partidos'));
    }

    public function create()
    {       
    }

    public function store()
    {       
    }

    public function edit($id)
    {                                  
        $user = \Auth::user()->id;         
        $matches = GrupoH::where('user_id','=',$user)->get();
        $partido = GrupoH::findOrFail($id);          
        return view('quiniela.grupo_h.edit', compact('partido','matches'));
    }


    public function update(FillQuinielaRequest $request,  $id)
    {
        $partido = GrupoH::find($id);
       
        if(!$partido->activeGame == 1){
            $data = $request->all();
            $user_id = $data["user_id"];
            $partido->goles_A = $data["goles_A"];
            $partido->goles_B = $data["goles_B"];
            $partido->minGolesA = $data["minGolesA"];
            $partido->minGolesA = $data["minGolesA"];
            $partido->minGolesB = $data["minGolesB"];        

            $resul = $partido->save();
                if ($resul) {
                    return back()->with('info', 'Usuario Actualizado');
                } else {
                    return view("mensajes.rechazado")->with("msj", "hubo un error vuelva a intentarlo");
                }
        } else {
            return back()->with('info-err', 'Partido está cerrado, No se puede actualizar');
        }
    }

    public function show()
    {       
    }
}

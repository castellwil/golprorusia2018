<?php

namespace App\Http\Controllers;
use App\GrupoA;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['privacity', 'terms']]);
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user()->id;  
        if(!$this->comprobarTabla($user) ){
            $mostrarBoton = true;
        } else {
            $mostrarBoton = false;
        }
        $codigoDln ="DLN18061015";
        return view('home', compact('mostrarBoton', 'codigoDln'));
    }

    public function privacity()
    {
        return view('privacidad');
    }

    public function terms()
    {
        return view('terminos');
    }

    public function comprobarTabla($user)
    {      
        $user = \Auth::user()->id; 
        $partidos = GrupoA::where('user_id','=',$user)->get();               
        if(count($partidos)> 0){
            return redirect('/quiniela')->with('info', 'Ya tienes la quiniela lista para llenar');
        }        
    }

    public function contacto()
    {
        return view('contacto');
    }
}

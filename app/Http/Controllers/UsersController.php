<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Policies\UserPolicy;
use App\Http\Requests\updateUserRequest;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
        $this->middleware('roles:admin', ['except'=> ['edit', 'update', 'show']]);

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);        
        return view( 'users.show', compact('user') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $this->authorize($user);
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $this->authorize($user);
        $user->update($request->all());
        return back()->with('info', 'Usuario Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);   
        $this->authorize($user);     
        $user->update(['active' => false]);
        return redirect('/')->with('info', 'Usuario eliminado');
    }

    public function subir_imagen_usuario(Request $request)
	{
	    $id = $request->input('img');
		$archivo = $request->file('archivo');
        $input  = array('image' => $archivo) ;
        $reglas = array('image' => 'required|image|mimes:jpeg,jpg,bmp,png,gif|max:900');

        $validacion = Validator::make($input,  $reglas);

        if ($validacion->fails())
        {
          return view("mensajes.msj_rechazado")->with("msj","El archivo no es una imagen valida");
        }
        else
        {
	        $nombre_original = $archivo->getClientOriginalName();
			$extension = $archivo->getClientOriginalExtension();
            $nuevo_nombre = "userimagen-".$id.".".$extension;
            
		    $r1 = Storage::disk('fotografias')->put($nuevo_nombre,  \File::get($archivo) );
		    $rutadelaimagen = "fotografias/".$nuevo_nombre;
	    
		    if ($r1){
			    $usuario = User::find($id);
			    $usuario->imagenurl = $rutadelaimagen;
			    $r2 = $usuario->save();
		        return view("mensajes.msj_correcto")->with("msj","Imagen agregada correctamente");
            }
        }
    }

    public function cambiar_password(Request $request)
    {
        $id = $request->input("id_usuario_password");
        $email = $request->input("email_usuario");
        $password = $request->input("password_usuario");
        $usuario = User::find($id);
        $usuario->email = $email;
        $usuario->password = bcrypt($password);
        $r = $usuario->save();

        if ($r) {
            return view("mensajes.msj_correcto")->with("msj", "password actualizado");
        } else {
            return view("mensajes.msj_rechazado")->with("msj", "Error al actualizar el password");
        }
    }
}

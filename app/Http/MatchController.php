<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiniela;
use Carbon\Carbon;

class MatchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        $partidos = Quiniela::orderBy('created_at','Desc')->get();            
        return view('quiniela.oficial.index', compact('partidos'));
    }

    public function create()
    {
        return view('quiniela.oficial.create');
    }

    public function store(Request $request)
    {
            $match = $request->all();

            $cuenta = new Quiniela;
            $cuenta->code =  $match['code'];
            $cuenta->grupoFase = $match['grupoFase'];
            $cuenta->equipo_A = $match['equipo_A'];;            
            $cuenta->equipo_B = $match['equipo_B']; 
            $cuenta->goles_A = $match['goles_A']; 
            $cuenta->goles_B = $match['goles_B']; 
            $cuenta->minGolesA = $match['minGolesA']; 
            $cuenta->minGolesB = $match['minGolesB']; 
            $cuenta->activeGame = 1;            
            $cuenta->visible = 1;
            $cuenta->user_id =  1; 
            $cuenta->save();            
        return redirect('lista_partidos_oficiales')->with('info', 'Partido agregado');
    }

    public function edit($id)
    {
        $partido = Quiniela::find($id);
        return view("quiniela.oficial.editar" , compact("partido"));       
    }

    public function update(Request $request, $id)
    {
        $partido = Quiniela::findOrFail($id);
        $partido->update($request->all());
        return redirect('lista_partidos_oficiales')->with('info', 'Partido Actualizado');
    }

    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiniela extends Model
{
    protected $table = "quinielas";
    protected $fillable = [
        'code', 'grupoFase', 'equipo_A', 'equipo_B', 'goles_A', 'goles_B', 'minGolesA', 'minGolesB', 'hourGame', 'activeGame'
    ];
    
    protected $dates = ['hourGame'];
    // protected $dateFormat = 'Y-m-d H:i:s';

    public function bandera($cadena)
    {
        $original =   'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificada = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($original), $modificada);
        $cadena = strtolower($cadena);
        $cadena = utf8_encode($cadena);
        return substr($cadena, 0,3);
    }

    public static function chooseGroup($grupo)
    {
        switch ($grupo) {
            case "grupoa":
                    $grupo = "Grupo A";
                break;
            case "grupob":
                $grupo = "Grupo B";
            break;
            case "grupoc":
                $grupo = "Grupo C";
            break;
            case "grupod":
                $grupo = "Grupo D";
            break;
            case "grupoe":
                $grupo = "Grupo E";
            break;
            case "grupof":
                $grupo = "Grupo F";
            break;
            case "grupog":
                $grupo = "Grupo G";
            break;
            case "grupoh":
                    $grupo = "Grupo H";
                break;
            default: '';          
        }  
        return $grupo;
    }

    public  function comprobarTabla($user)
    {      
        $user = \Auth::user()->id; 
        $partidos = GrupoA::where('user_id','=',$user)->get();               
        if(count($partidos)> 0){
            return redirect('/quiniela')->with('info', 'Ya tienes la quiniela lista para llenar');
        }
        
    }

        
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoA extends Model
{
    protected $table = "grupo_a";
    protected $fillable = [
        'code', 'grupoFase', 'equipo_A', 'equipo_B', 'goles_A', 'goles_B', 'minGolesA', 'minGolesB', 'hourGame', 'activeGame'
    ];
    
    protected $dates = ['hourGame'];
    // protected $dateFormat = 'Y-m-d H:i:s';

    public function bandera($cadena)
    {
        $original =   'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificada = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($original), $modificada);
        $cadena = strtolower($cadena);
        $cadena = utf8_encode($cadena);
        return substr($cadena, 0,3);
    }

    public  function comprobarTabla($user)
    {      
        $user = \Auth::user()->id; 
        $partidos = GrupoA::where('user_id','=',$user)->get();               
        if(count($partidos)> 0){
            return redirect('/quiniela')->with('info', 'Ya tienes la quiniela lista para llenar');
        }
        
    }
    public function LastLetter($cadena)
    {
        $original =   'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificada = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($original), $modificada);
        $cadena = strtoupper($cadena);
        $cadena = utf8_encode($cadena);
        return substr($cadena, -1);
    }   

    public function user()
    {
        return $this->belongsTo(User::class);
    }
   
}

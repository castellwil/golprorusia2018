<?php
// Route::get('roles', function(){
//     return \App\Role::with('user')->get();
// });

Route::get('/', function () { return view('welcome'); });
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/privacity', 'HomeController@privacity');
Route::get('/terms', 'HomeController@terminos');
Route::get('activate/{token}', 'ActivationTokenController@activate')->name('activation');

Auth::routes();

Route::resource('lista_partidos_oficiales', 'MatchController');
Route::resource('usuarios', 'UsersController');

Route::get('config14605145pro', 'QuinielaController@configApp')->name('configApp');
Route::get('config1968pro', 'QuinielaController@configQuinielas')->name('configQuinielas');
Route::get('cerrarpartido/{grupo}/{code}/{active}', 'QuinielaController@CerrarPartido');

Route::get('isadmintrue', 'ListaUsuariosController@isadminRole');

Route::get('ActualizarPuntosPartido/{code}', 'PointController@ActualizarPuntosPartido');

Route::get('tabla2', 'PointController@tabla2');

Route::get('configtablaPosiciones/{desde?}/{hasta?}', 'PositionController@basePosition');
Route::get('ActualizarPuntosPartido/{code}', 'PointController@procesarPartidoyCargarPuntos');
Route::get('tabla', 'PositionController@index');




Route::resource('quiniela', 'QuinielaController');
Route::resource('grupoA', 'GrupoAController');
Route::resource('grupoB', 'GrupoBController');
Route::resource('grupoC', 'GrupoCController');
Route::resource('grupoD', 'GrupoDController');
Route::resource('grupoE', 'GrupoEController');
Route::resource('grupoF', 'GrupoFController');
Route::resource('grupoG', 'GrupoGController');
Route::resource('grupoH', 'GrupoHController');

Route::get('resumen/{usuario?}', 'QuinielaController@index');


Route::get('lista', 'ListaUsuariosController@index');
Route::get('favoritas', 'ListaUsuariosController@favoritas');












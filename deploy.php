<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'golpro');

// Project repository
set('repository', 'git@bitbucket.org:castellwil/golprorusia2018.git');

// Windows Compatibility
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
    set('git_tty', false);
    set('ssh_multiplexing', false);
}

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);


// Hosts

host('buzzews.com')
    ->user('admin')
    ->identityFile('~/.ssh/id_rsa')
    ->set('deploy_path', '/home/admin/web/buzzews.com/public_html/');   
    
// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

//before('deploy:symlink', 'artisan:migrate');


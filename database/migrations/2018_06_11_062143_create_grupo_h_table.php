<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrupoHTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_h', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',10)->nullable();
            $table->string('grupoFase')->nullable();
            $table->string('equipo_A')->nullable();
            $table->string('flat_A')->nullable();
            $table->string('equipo_B')->nullable();
            $table->string('flat_B')->nullable();
            $table->integer('goles_A')->nullable();
            $table->integer('goles_B')->nullable();
            $table->string('minGolesA')->nullable();
            $table->string('minGolesB')->nullable();
            $table->datetime('hourGame')->nullable();
            $table->boolean('activeGame')->default(true);
            $table->boolean('visible')->default(true);            
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo_h');
    }
}

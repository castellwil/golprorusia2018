<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuinielasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quinielas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',10)->nullable();
            $table->string('grupoFase');
            $table->string('equipo_A');
            $table->string('equipo_B');
            $table->integer('goles_A')->nullable();
            $table->integer('goles_B')->nullable();
            $table->string('minGolesA')->nullable();
            $table->string('minGolesB')->nullable();
            $table->datetime('hourGame')->nullable();
            $table->boolean('activeGame')->default(true);
            $table->boolean('visible')->default(true);
            $table->unsignedInteger('user_id');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quinielas');
    }
}

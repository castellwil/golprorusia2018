<header class="main-header">
    @include('layouts.partials.logo')

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">           
                @include('layouts.partials.userinfo')
            </ul>
        </div>
    </nav>
</header>

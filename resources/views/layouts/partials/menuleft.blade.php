    <ul class="sidebar-menu" data-widget="tree">
        <li class="header" >Menú Admin</li> 
@if( auth()->user()->hasRoles(['admin']) )
        
    <li class="treeview">
        <a href="#"><i class="fa fa-dashboard"></i> <span>usuarios</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>            
        <ul class="treeview-menu">
            <li><a href="{{route('usuarios.index')}}"><i class="fa fa-link"></i>Lista</a></li>
        </ul>
    </li>

    <li class="treeview">
        <a href="#"><i class="fa fa-link"></i> <span>Partidos Ofic.</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>            
        <ul class="treeview-menu">
            <li><a href="{{route('lista_partidos_oficiales.index')}}"><i class="fa fa-link"></i> <span>Lista</span></a></li>
            <li><a href="{{route('lista_partidos_oficiales.create')}}"><i class="fa fa-link"></i> <span>Agregar </span></a></li>
            <li><a href="#"><i class="fa fa-link"></i> <span>Eliminar</span></a></li>
        </ul>
    </li>    
        
@endif


    <li class=""><a href="/home" ><i class="fa fa-table"></i>Reglas de la Quiniela  </a> </li>

    <li class="treeview active menu-open">
        
        <a href="#"><i class="fa fa-dashboard"></i> <span>Fase de grupos</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>            
        <ul class="treeview-menu">       
            <li class=""><a href="/grupoA" ><i class="fa fa-table"></i> Grupo A  </a> </li>
            <li class=""><a href="/grupoB" ><i class="fa fa-table"></i> Grupo B  </a> </li>
            <li class=""><a href="/grupoC" ><i class="fa fa-table"></i> Grupo C  </a> </li>
            <li class=""><a href="/grupoD" ><i class="fa fa-table"></i> Grupo D  </a> </li>
            <li class=""><a href="/grupoE" ><i class="fa fa-table"></i> Grupo E  </a> </li>
            <li class=""><a href="/grupoF" ><i class="fa fa-table"></i> Grupo F  </a> </li>
            <li class=""><a href="/grupoG" ><i class="fa fa-table"></i> Grupo G  </a> </li>
            <li class=""><a href="/grupoH" ><i class="fa fa-table"></i> Grupo H  </a> </li>
            <li class=""><a href="/resumen" ><i class="fa fa-table"></i><b>Resumen </b>  </a> </li>            
        </ul>
    </li>
 
    <li class="treeview active menu-open">
        <a href="#"><i class="fa fa-soccer-ball-o"></i> <span>Ver Quinielas.</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>            
        <ul class="treeview-menu"> 
            <li>
                <a href="/lista"><i class="fa fa-file-text-o"></i> <span>Participantes</span></a>              
            </li> 
            <li>
                <a href="/tabla"><i class="fa fa-file-text-o"></i> <span>Tabla de Posiciones</span></a>              
            </li> 
            
            {{--  <li>
                <a href="/favoritas"><i class="fa fa-file-text-o"></i> <span>Quinielas favoritas</span></a>              
            </li>    --}}
        </ul>
    </li>
         

</ul>

    
        <li class="dropdown user user-menu"> 
        @auth            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">             
              <img src="{{ auth()->user()->img }}" class="user-image" alt="User Image"> 
                         
              <span class="hidden-xs"> {{ auth()->user()->nombre }} </span>
            </a>
            <ul class="dropdown-menu">            
              <li class="user-header">
                <img src="{{ auth()->user()->img }}" class="img-circle" alt="User Image">
                <p> Player {{ auth()->user()->id }} - {{ auth()->user()->nombre }} {{ auth()->user()->apellido }} </p>
              </li>             
              <li class="user-body">
                <div class="row">
                  
                </div>                
              </li>             
              <li class="user-footer">
                <div class="pull-left">
                  <a href="/usuarios/{{ auth()->id() }}/edit" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <form method="POST" action="{{ route('logout') }}">
                    {{ csrf_field() }}
                    <button  class="btn btn-default btn-flat">Cerrar sesión</button>
                  </form>
                </div>
              </li>
            </ul>
          </li>
          @endauth
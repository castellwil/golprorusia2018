    <aside class="main-sidebar">
        <section class="sidebar">
      
            <div class="user-panel">
               @auth
                <div class="pull-left image">
                <img src="{{ auth()->user()->img }}" class="img-circle" alt="User Image">
                </div>
               
                <div class="pull-left info">
               
                <p>{{ auth()->user()->nombre }}</p>         
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
                 @endauth
            </div>
    
            
            @include('layouts.partials.menuleft')
                 
        </section>
    </aside>
    
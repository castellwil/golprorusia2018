 <footer class="main-footer">

    <div class="pull-right hidden-xs">
      Diario La Nación
    </div>
    <a href="privacity">Privacy Policy</a> - 
    <a href="terms">Terms</a>
    <strong>Copyright &copy; 2016 <a href="#">DLN - GolPro</a>.</strong> All rights reserved.
  </footer>

  <script src="{{asset('js/web.js')}}"></script>
  <script src="https://golpro.test/js/app.js"></script>
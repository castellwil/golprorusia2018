<!DOCTYPE html>
<html>
    @include('layouts.partials.head')
    <body class="hold-transition skin-purple-light sidebar-mini">
        <div class="wrapper">

            @include('layouts.partials.top')

            <aside class="main-sidebar">
                @include('layouts.partials.leftbar')
            </aside>

            <div class="content-wrapper">

                <section class="content-header">  </section>
                <section class="content container-fluid" id="contenido_principal">
                    @yield('content')
                </section>
                @include('layouts.partials.cargando')
            </div>
        </div>
    @include('layouts.partials.footer')
    </body>
</html>

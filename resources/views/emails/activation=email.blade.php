@component('mail::message')
# Activación para crear cuenta en la Quiniela Golpro de Diario La Nación

Sigue este link para activar tu cuenta.

@component('mail::button', ['url' => route('activation', $user->token)])
Activa tu cuenta
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent

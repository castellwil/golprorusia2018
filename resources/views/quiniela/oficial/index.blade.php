@extends('layouts.master')
@section('content')
<h1>Resumen de quiniela - Fase de grupo  </h1>
   
   @if( session()->has('info') )
    <div class="alert alert-success">{{ session('info') }}</div> 
    @endif

    <div class="row">
            
    @foreach($partidos as $partido)
    
        <div class="col-sm-6 col-lg-2">
            <div class="box box-info ">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $partido->grupoFase }} </h3>  <small>  </small> 
                    
                </div>              

                <div id="Infopartido{{ $partido->code }}"></div>
                
                <form method="POST" action="" id="{{ $partido->id }}" class="form-horizontal form_entrada  " > 
                    <div class="box-body">
                        
                      <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">
                                {{ $partido->equipo_A }} 
                            </label>
                          
                            <div class="col-sm-8">                              
                                <h2>{{ $partido->goles_A }}</h2>                           
                                <small>{{$partido->minGolesA }}</small>                                   
                            </div>
                      </div>                        
                    <hr>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">
                                {{ $partido->equipo_B }} 
                            </label>

                            <div class="col-sm-8">                               
                              <h2>{{ $partido->goles_B }}</h2>
                                <small>{{ $partido->minGolesB }}</small>                                                      
                            </div>
                        </div>             

                    </div>                    
                    <div class="box-footer">
                        <a class="btn btn-info btn-xs" href="{{route('lista_partidos_oficiales.edit', $partido->id)}}">Editar</a>
                        <a class="btn btn-danger btn-xs pull-right" href="ActualizarPuntosPartido/{{$partido->code}}">Procesar</a>
                        
                        

                    </div>                
                </form>                 
            </div>
        </div> 
        @endforeach
    </div>

    

 @stop
@extends('layouts.master')
@section('content')<h1>Fase de Grupos  </h1>
   @if( session()->has('info') )
    <div class="alert alert-success">{{ session('info') }}</div> 
    @endif    

    <div class="row">
   
        <div class="col-sm-6">
            <div class="box box-info ">
                <div class="box-header with-border">
                    <h3 class="box-title">
                            @if($partido->activeGame == 1)
                            <div class="text-danger">  Cerrado </div>
                            @endif
                        {{ $partido->grupoFase }} </h3> | <small> {{ $partido->hourGame }} Horas </small>
                </div>
              

                <div id="Infopartido{{ $partido->code }}"></div>
                
                <form method="POST" action="{{ route('lista_partidos_oficiales.update', $partido->id) }}" id="{{ $partido->id }}" class="form-horizontal form_entrada  " >    
                    {!!   method_field('PUT') !!}
                    {!! csrf_field() !!} 
                    <div class="box-body">
                      <input type="hidden" class="form-control" name="user_id"  value=" {{auth()->user()->id  }} " > 
                      <div class="form-group">                       
                        <input type="hidden" class="form-control" name="code"  value=" {{ $partido->code }} ">                        
                      </div>
                      <div class="form-group">                        
                        <input type="hidden" class="form-control" name="grupoFase"  value=" {{ $partido->grupoFase }} ">                 
                      </div>
                        
                      <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4 control-label">{{ $partido->equipo_A }} <img src="/img/{{ $partido->bandera($partido->equipo_A) }}.png"></label>
                          <input type="hidden" class="form-control" name="equipo_A"  value=" {{ $partido->equipo_A }} "> 
                          <div class="col-sm-8">
                              <small>Cuántos goles hace</small>
                              <input type="text" class="form-control" name="goles_A"  value=" {{ $partido->goles_A }} " autocomplete="off"> 
                              {!! $errors->first('goles_A', '<span class=error>:message</span>') !!}
                              <small>Introduce los minutos en los que se podría dar cada gol, sepáralos por coma(,) si es cero deja en blanco</small>
                              <input type="text" class="form-control" name="minGolesA"   placeholder="ejemplo 5,27,90"  value=" {{ $partido->minGolesA }}" autocomplete="off">                                           
                          </div>
                      </div>                        
                    <hr>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">{{ $partido->equipo_B }} <img src="/img/{{ $partido->bandera($partido->equipo_B) }}.png"></label>
                            <input type="hidden" class="form-control" name="equipo_B"  value=" {{ $partido->equipo_B }} ">
                            <div class="col-sm-8">
                                <small>Cuántos goles hace</small>
                                <input type="text" class="form-control" name="goles_B"  value=" {{ $partido->goles_B }} " autocomplete="off"> 
                                {!! $errors->first('goles_B', '<span class=error>:message</span>') !!}
                                <small>Introduce los minutos en los que se podría dar cada gol, sepáralos por coma(,) si es cero deja en blanco</small>
                                <input type="text" class="form-control" name="minGolesB"   value= " {{ $partido->minGolesB }}" placeholder="ejemplo 5,27,90"  autocomplete="off"> 
                                                      
                            </div>
                        </div>             

                    </div>
                    
                    <div class="box-footer">
                                      
                        <button type="submit" class="btn btn-info pull-right">Guardar</button>
                    </div>
                
                </form>
                 
                  
            </div>
        </div>
 
       
    </div>
@stop
 
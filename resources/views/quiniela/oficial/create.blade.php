@extends('layouts.master')
@section('content')
   <h1>Fase de Grupos  </h1>
   @if( session()->has('info') )
    <div class="alert alert-success">{{ session('info') }}</div> 
    @endif
   

    <div class="row">
   
        <div class="col-sm-6">
            <div class="box box-info ">
                <div class="box-header with-border">
                    <h3 class="box-title">Agregar partido oficial </h3>  <small>  </small>
                </div>  
                
                
                <form method="POST" action="{{route('lista_partidos_oficiales.store')}}"  class="form-horizontal" >        
                    {!! csrf_field() !!}   
                    <div class="box-body">
                      <input type="text" class="form-control" name="user_id"  value="1" > 
                      <div class="form-group">                       
                        <input type="text" class="form-control" name="code"  placeholder="code">                        
                      </div>
                      <div class="form-group">                        
                        <input type="text" class="form-control" name="grupoFase"  placeholder="Fase">                 
                      </div>
                        
                      <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4 control-label">Equipo A</label>
                          <input type="text" class="form-control" name="equipo_A"  placeholder="Equipo A"> 
                          <div class="col-sm-8">
                              <small>Cuántos goles hace</small>
                              <input type="number" class="form-control" name="goles_A"  value=" {{ old('goles_A') }} " autocomplete="off"> 
                              {!! $errors->first('goles_A', '<span class=error>:message</span>') !!}
                              <small>Introduce los minutos en los que se podría dar cada gol, sepáralos por coma(,) si es cero deja en blanco</small>
                              <input type="text" class="form-control" name="minGolesA"   placeholder="ejemplo 5,27,90"  autocomplete="off">                                           
                          </div>
                      </div>                        
                    <hr>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Equipo B</label>
                            <input type="text" class="form-control" name="equipo_B"  placeholder="Equipo B">
                            <div class="col-sm-8">
                                <small>Cuántos goles hace</small>
                                <input type="number" class="form-control" name="goles_B"  value=" {{ old('goles_B') }} " autocomplete="off"> 
                                {!! $errors->first('goles_B', '<span class=error>:message</span>') !!}
                                <small>Introduce los minutos en los que se podría dar cada gol, sepáralos por coma(,) si es cero deja en blanco</small>
                                <input type="text" class="form-control" name="minGolesB"   placeholder="ejemplo 5,27,90"  autocomplete="off"> 
                                                      
                            </div>
                        </div>             

                    </div>
                    
                    <div class="box-footer">
                                      
                        <button type="submit" class="btn btn-info pull-right">Guardar</button>
                    </div>
                
                </form>
                 
                  
            </div>
        </div>
 
      
    </div>

    @stop
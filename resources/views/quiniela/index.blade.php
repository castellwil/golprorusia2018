@extends('layouts.master')
@section('content')
<h1>Resumen de quiniela - Fase de grupo  </h1>
   
   @if( session()->has('info') )
    <div class="alert alert-success">{{ session('info') }}</div> 
    @endif

    <div class="row">
            
    @foreach($partidosA as $partidoA)
    
        <div class="col-sm-6 col-lg-2">
            <div class="box box-info ">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $partidoA->grupoFase }} </h3>  <small> {{ $partidoA->hourGame->format('d M Y | H:i:s ') }}  </small> 
                    
                </div>              

                <div id="Infopartido{{ $partidoA->code }}"></div>
                
                <form method="POST" action="editar_partidos" id="{{ $partidoA->id }}" class="form-horizontal form_entrada  " >               <div class="box-body">
                        
                      <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">
                                {{ $partidoA->equipo_A }} <img src="/img/{{ $partidoA->flat_A }}.png">
                            </label>
                          
                            <div class="col-sm-8">                              
                                <h2>{{ $partidoA->goles_A }}</h2>                           
                                <small>{{$partidoA->minGolesA }}</small>                                   
                            </div>
                      </div>                        
                    <hr>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">
                                {{ $partidoA->equipo_B }}  <img src="/img/{{ $partidoA->flat_B }}.png">
                            </label>

                            <div class="col-sm-8">                               
                              <h2>{{ $partidoA->goles_B }}</h2>
                                <small>{{ $partidoA->minGolesB }}</small>                                                      
                            </div>
                        </div>             

                    </div>                    
                    <div class="box-footer"> {{$partidoA->user->nombre}} {{$partidoA->user->apellido}} </div>                
                </form>                 
            </div>
        </div> 
        @endforeach
    </div>

    <div class="row">
            
            @foreach($partidosB as $partidoB)
            
                <div class="col-sm-6 col-lg-2">
                    <div class="box box-info ">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $partidoB->grupoFase }} </h3>  <small> {{ $partidoB->hourGame->format('d M Y | H:i:s ') }}  </small> 
                            {{$partidoB->nombre}}
                        </div>              
        
                        <div id="Infopartido{{ $partidoB->code }}"></div>
                        
                        <form method="POST" action="editar_partidos" id="{{ $partidoB->id }}" class="form-horizontal form_entrada  " >               <div class="box-body">
                                
                              <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 control-label">
                                        {{ $partidoB->equipo_A }} <img src="/img/{{ $partidoB->flat_A }}.png">
                                    </label>
                                  
                                    <div class="col-sm-8">                              
                                        <h2>{{ $partidoB->goles_A }}</h2>                           
                                        <small>{{$partidoB->minGolesA }}</small>                                   
                                    </div>
                              </div>                        
                            <hr>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 control-label">
                                        {{ $partidoB->equipo_B }}  <img src="/img/{{ $partidoB->flat_B }}.png">
                                    </label>
        
                                    <div class="col-sm-8">                               
                                      <h2>{{ $partidoB->goles_B }}</h2>
                                        <small>{{ $partidoB->minGolesB }}</small>                                                      
                                    </div>
                                </div>             
        
                            </div>                    
                            <div class="box-footer"> {{$partidoB->user->nombre}} {{$partidoB->user->apellido}} </div>                
                        </form>                 
                    </div>
                </div> 
                @endforeach
            </div>

            <div class="row">
            
                    @foreach($partidosC as $partidoC)
                    
                        <div class="col-sm-6 col-lg-2">
                            <div class="box box-info ">
                                <div class="box-header with-border">
                                    <h3 class="box-title">{{ $partidoC->grupoFase }} </h3>  <small> {{ $partidoC->hourGame->format('d M Y | H:i:s ') }}  </small> 
                                    {{$partidoC->nombre}}
                                </div>              
                
                                <div id="Infopartido{{ $partidoC->code }}"></div>
                                
                                <form method="POST" action="editar_partidos" id="{{ $partidoC->id }}" class="form-horizontal form_entrada  " >               <div class="box-body">
                                        
                                      <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">
                                                {{ $partidoC->equipo_A }} <img src="/img/{{ $partidoC->flat_A }}.png">
                                            </label>
                                          
                                            <div class="col-sm-8">                              
                                                <h2>{{ $partidoC->goles_A }}</h2>                           
                                                <small>{{$partidoC->minGolesA }}</small>                                   
                                            </div>
                                      </div>                        
                                    <hr>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">
                                                {{ $partidoC->equipo_B }}  <img src="/img/{{ $partidoC->flat_B }}.png">
                                            </label>
                
                                            <div class="col-sm-8">                               
                                              <h2>{{ $partidoC->goles_B }}</h2>
                                                <small>{{ $partidoC->minGolesB }}</small>                                                      
                                            </div>
                                        </div>             
                
                                    </div>                    
                                    <div class="box-footer"> {{$partidoC->user->nombre}} {{$partidoC->user->apellido}} </div>               
                                </form>                 
                            </div>
                        </div> 
                        @endforeach
                    </div>

                    <div class="row">
            
                            @foreach($partidosD as $partidoD)
                            
                                <div class="col-sm-6 col-lg-2">
                                    <div class="box box-info ">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">{{ $partidoD->grupoFase }} </h3>  <small> {{ $partidoD->hourGame->format('d M Y | H:i:s ') }}  </small> 
                                            {{$partidoD->nombre}}
                                        </div>              
                        
                                        <div id="Infopartido{{ $partidoD->code }}"></div>
                                        
                                        <form method="POST" action="editar_partidos" id="{{ $partidoD->id }}" class="form-horizontal form_entrada  " >               <div class="box-body">
                                                
                                              <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-4 control-label">
                                                        {{ $partidoD->equipo_A }} <img src="/img/{{ $partidoD->flat_A }}.png">
                                                    </label>
                                                  
                                                    <div class="col-sm-8">                              
                                                        <h2>{{ $partidoD->goles_A }}</h2>                           
                                                        <small>{{$partidoD->minGolesA }}</small>                                   
                                                    </div>
                                              </div>                        
                                            <hr>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-4 control-label">
                                                        {{ $partidoD->equipo_B }}  <img src="/img/{{ $partidoD->flat_B }}.png">
                                                    </label>
                        
                                                    <div class="col-sm-8">                               
                                                      <h2>{{ $partidoD->goles_B }}</h2>
                                                        <small>{{ $partidoD->minGolesB }}</small>                                                      
                                                    </div>
                                                </div>             
                        
                                            </div>                    
                                            <div class="box-footer"> {{$partidoD->user->nombre}} {{$partidoD->user->apellido}} </div>              
                                        </form>                 
                                    </div>
                                </div> 
                                @endforeach
                            </div>

                            <div class="row">
            
                                    @foreach($partidosE as $partidoE)
                                    
                                        <div class="col-sm-6 col-lg-2">
                                            <div class="box box-info ">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">{{ $partidoE->grupoFase }} </h3>  <small> {{ $partidoE->hourGame->format('d M Y | H:i:s ') }}  </small> 
                                                    {{$partidoE->nombre}}
                                                </div>              
                                
                                                <div id="Infopartido{{ $partidoE->code }}"></div>
                                                
                                                <form method="POST" action="editar_partidos" id="{{ $partidoE->id }}" class="form-horizontal form_entrada  " >               <div class="box-body">
                                                        
                                                      <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-4 control-label">
                                                                {{ $partidoE->equipo_A }} <img src="/img/{{ $partidoE->flat_A }}.png">
                                                            </label>
                                                          
                                                            <div class="col-sm-8">                              
                                                                <h2>{{ $partidoE->goles_A }}</h2>                           
                                                                <small>{{$partidoE->minGolesA }}</small>                                   
                                                            </div>
                                                      </div>                        
                                                    <hr>
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-4 control-label">
                                                                {{ $partidoE->equipo_B }}  <img src="/img/{{ $partidoE->flat_B }}.png">
                                                            </label>
                                
                                                            <div class="col-sm-8">                               
                                                              <h2>{{ $partidoE->goles_B }}</h2>
                                                                <small>{{ $partidoE->minGolesB }}</small>                                                      
                                                            </div>
                                                        </div>             
                                
                                                    </div>                    
                                                   <div class="box-footer"> {{$partidoE->user->nombre}} {{$partidoE->user->apellido}} </div>               
                                                </form>                 
                                            </div>
                                        </div> 
                                        @endforeach
                                    </div>

                                    <div class="row">
            
                                            @foreach($partidosF as $partidoF)
                                            
                                                <div class="col-sm-6 col-lg-2">
                                                    <div class="box box-info ">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">{{ $partidoF->grupoFase }} </h3>  <small> {{ $partidoF->hourGame->format('d M Y | H:i:s ') }}  </small> 
                                                            {{$partidoF->nombre}}
                                                        </div>              
                                        
                                                        <div id="Infopartido{{ $partidoF->code }}"></div>
                                                        
                                                        <form method="POST" action="editar_partidos" id="{{ $partidoF->id }}" class="form-horizontal form_entrada  " >               <div class="box-body">
                                                                
                                                              <div class="form-group">
                                                                    <label for="inputEmail3" class="col-sm-4 control-label">
                                                                        {{ $partidoF->equipo_A }} <img src="/img/{{ $partidoF->flat_A }}.png">
                                                                    </label>
                                                                  
                                                                    <div class="col-sm-8">                              
                                                                        <h2>{{ $partidoF->goles_A }}</h2>                           
                                                                        <small>{{$partidoF->minGolesA }}</small>                                   
                                                                    </div>
                                                              </div>                        
                                                            <hr>
                                                                <div class="form-group">
                                                                    <label for="inputEmail3" class="col-sm-4 control-label">
                                                                        {{ $partidoF->equipo_B }}  <img src="/img/{{ $partidoF->flat_B }}.png">
                                                                    </label>
                                        
                                                                    <div class="col-sm-8">                               
                                                                      <h2>{{ $partidoF->goles_B }}</h2>
                                                                        <small>{{ $partidoF->minGolesB }}</small>                                                      
                                                                    </div>
                                                                </div>             
                                        
                                                            </div>                    
                                                            <div class="box-footer"> {{$partidoF->user->nombre}} {{$partidoF->user->apellido}} </div>                
                                                        </form>                 
                                                    </div>
                                                </div> 
                                                @endforeach
                                            </div>



                                            <div class="row">
            
                                                    @foreach($partidosG as $partidoG)
                                                    
                                                        <div class="col-sm-6 col-lg-2">
                                                            <div class="box box-info ">
                                                                <div class="box-header with-border">
                                                                    <h3 class="box-title">{{ $partidoG->grupoFase }} </h3>  <small> {{ $partidoG->hourGame->format('d M Y | H:i:s ') }}  </small> 
                                                                    {{$partidoG->nombre}}
                                                                </div>              
                                                
                                                                <div id="Infopartido{{ $partidoG->code }}"></div>
                                                                
                                                                <form method="POST" action="editar_partidos" id="{{ $partidoG->id }}" class="form-horizontal form_entrada  " >               <div class="box-body">
                                                                        
                                                                      <div class="form-group">
                                                                            <label for="inputEmail3" class="col-sm-4 control-label">
                                                                                {{ $partidoG->equipo_A }} <img src="/img/{{ $partidoG->flat_A }}.png">
                                                                            </label>
                                                                          
                                                                            <div class="col-sm-8">                              
                                                                                <h2>{{ $partidoG->goles_A }}</h2>                           
                                                                                <small>{{$partidoG->minGolesA }}</small>                                   
                                                                            </div>
                                                                      </div>                        
                                                                    <hr>
                                                                        <div class="form-group">
                                                                            <label for="inputEmail3" class="col-sm-4 control-label">
                                                                                {{ $partidoG->equipo_B }}  <img src="/img/{{ $partidoG->flat_B }}.png">
                                                                            </label>
                                                
                                                                            <div class="col-sm-8">                               
                                                                              <h2>{{ $partidoG->goles_B }}</h2>
                                                                                <small>{{ $partidoG->minGolesB }}</small>                                                      
                                                                            </div>
                                                                        </div>             
                                                
                                                                    </div>                    
                                                                    <div class="box-footer"> {{$partidoG->user->nombre}} {{$partidoG->user->apellido}} </div>                
                                                                </form>                 
                                                            </div>
                                                        </div> 
                                                        @endforeach
                                                    </div>


                                                    <div class="row">
            
                                                            @foreach($partidosH as $partidoH)
                                                            
                                                                <div class="col-sm-6 col-lg-2">
                                                                    <div class="box box-info ">
                                                                        <div class="box-header with-border">
                                                                            <h3 class="box-title">{{ $partidoH->grupoFase }} </h3>  <small> {{ $partidoH->hourGame->format('d M Y | H:i:s ') }}  </small> 
                                                                            {{$partidoH->nombre}}
                                                                        </div>              
                                                        
                                                                        <div id="Infopartido{{ $partidoH->code }}"></div>
                                                                        
                                                                        <form method="POST" action="editar_partidos" id="{{ $partidoH->id }}" class="form-horizontal form_entrada  " >               <div class="box-body">
                                                                                
                                                                              <div class="form-group">
                                                                                    <label for="inputEmail3" class="col-sm-4 control-label">
                                                                                        {{ $partidoH->equipo_A }} <img src="/img/{{ $partidoH->flat_A }}.png">
                                                                                    </label>
                                                                                  
                                                                                    <div class="col-sm-8">                              
                                                                                        <h2>{{ $partidoH->goles_A }}</h2>                           
                                                                                        <small>{{$partidoH->minGolesA }}</small>                                   
                                                                                    </div>
                                                                              </div>                        
                                                                            <hr>
                                                                                <div class="form-group">
                                                                                    <label for="inputEmail3" class="col-sm-4 control-label">
                                                                                        {{ $partidoH->equipo_B }}  <img src="/img/{{ $partidoH->flat_B }}.png">
                                                                                    </label>
                                                        
                                                                                    <div class="col-sm-8">                               
                                                                                      <h2>{{ $partidoH->goles_B }}</h2>
                                                                                        <small>{{ $partidoE->minGolesB }}</small>                                                      
                                                                                    </div>
                                                                                </div>             
                                                        
                                                                            </div>                    
                                                                            <div class="box-footer"> {{$partidoH->user->nombre}} {{$partidoH->user->apellido}} </div>                
                                                                        </form>                 
                                                                    </div>
                                                                </div> 
                                                                @endforeach
                                                            </div>

 @stop
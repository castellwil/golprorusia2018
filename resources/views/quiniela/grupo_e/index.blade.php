@extends('layouts.master')
@section('content')
<h1>Fase de Grupos  </h1>
   

    <div class="row">
    @foreach($partidos as $partido)
        <div class="col-sm-6 col-lg-2">
            <div class="box box-info ">
                <div class="box-header with-border">
                    <h3 class="box-title">
                            @if($partido->activeGame == 1)
                            <div class="text-danger">  Cerrado </div>
                            @endif
                        {{ $partido->grupoFase }} </h3>  <small> {{ $partido->hourGame->format('d M Y | H:i:s ') }}  </small>
                </div>
              

                <div id="Infopartido{{ $partido->code }}"></div>
                
                <form method="POST" action="grupoE/edit/$partido->id" id="{{ $partido->id }}" class="form-horizontal form_entrada  " >         <div class="box-body">
                      <input type="hidden" class="form-control" name="user_id"  value=" {{auth()->user()->id  }} " > 
                      <div class="form-group">                       
                        <input type="hidden" class="form-control" name="code"  value=" {{ $partido->code }} ">                        
                      </div>
                      <div class="form-group">                        
                        <input type="hidden" class="form-control" name="grupoFase"  value=" {{ $partido->grupoFase }} ">                 
                      </div>
                        
                      <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4 control-label">{{ $partido->equipo_A }}
                                <img src="/img/{{ $partido->flat_A }}.png">
                            </label>
                          <input type="hidden" class="form-control" name="equipo_A"  value=" {{ $partido->equipo_A }} "> 
                          <div class="col-sm-8">
                              
                             <h2> {{ $partido->goles_A }}</h2>
                         
                           
                            <small> {{$partido->minGolesA }}     </small>                                   
                          </div>
                      </div>                        
                    <hr>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">{{ $partido->equipo_B }} 
                                    <img src="/img/{{ $partido->flat_B }}.png">
                            </label>
                            <input type="hidden" class="form-control" name="equipo_B"  value=" {{ $partido->equipo_B }} ">
                            <div class="col-sm-8">
                               
                              <h2> {{ $partido->goles_B }} </h2>
                                
                               
                             <small> {{ $partido->minGolesB }} </small>
                                                      
                            </div>
                        </div>             

                    </div>
                    
                    <div class="box-footer">
                         
                         <a class="btn  btn-skin-green btn-xs pull-right" href="/grupoE/{{$partido->id}}/edit" >
                                <i class="fa fa-fw fa-eye"></i>Ver
                        </a>         
                        
                    </div>
                
                </form>
                 
                  
            </div>
        </div>
 
        @endforeach
    </div>

 @stop
@extends('layouts.master')
@section('content')<h1>Fase de Grupos  </h1>
   @if( session()->has('info') )
    <div class="alert alert-success">{{ session('info') }}</div> 
    @endif

    @if ( session()->has('info-err') )
    <div class="alert alert-danger">{{ session('info-err') }}</div>
    @endif

    @if(count($errors)> 0)
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <ul>
            @foreach($errors->all() as $error)
            <li>{!!$error!!}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @foreach($matches as $match)
        <a href="/grupoH/{{$match->id}}/edit" class="btn btn-info btn-flat">ir a {{$match->equipo_A}} - {{$match->equipo_B}}</a>       
    @endforeach

    <div class="row">
       
        <div class="col-sm-12">
            <div class="box box-info ">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $partido->grupoFase }} </h3> | <small> {{ $partido->hourGame }} Horas </small>
                </div>
                 <div id="Infopartido{{ $partido->code }}">
            </div>
                
                <form method="POST" action="{{route('grupoH.update', $partido->id )}}" id="{{ $partido->id }}" class="form-horizontal form_entrada  " >    
                    {!!   method_field('PUT') !!}
                    {!! csrf_field() !!} 
                    <div class="box-body">
                      <input type="hidden" class="form-control" name="user_id"  value=" {{auth()->user()->id  }} " > 
                      <div class="form-group">                       
                        <input type="hidden" class="form-control" name="code"  value=" {{ $partido->code }} ">                        
                      </div>
                      <div class="form-group">                        
                        <input type="hidden" class="form-control" name="grupoFase"  value=" {{ $partido->grupoFase }} ">                 
                      </div>
                        
                      <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4 control-label">{{ $partido->equipo_A }} 
                                <img src="/img/{{ $partido->flat_A }}.png">
                            </label>
                          <input type="hidden" class="form-control" name="equipo_A"  value=" {{ $partido->equipo_A }} "> 
                          <div class="col-sm-8">
                              <small>Cuántos goles hace</small>
                              <input type="text" class="form-control" name="goles_A"  value=" {{ $partido->goles_A }} " autocomplete="off"> 
                              {!! $errors->first('goles_A', '<span class=error>:message</span>') !!}
                              <small>Introduce los minutos en los que se podría dar cada gol, sepáralos por coma(,) si es cero deja en blanco</small>
                              <input type="text" class="form-control" name="minGolesA"   placeholder="ejemplo 5,27,90"  value=" {{ $partido->minGolesA }}" autocomplete="off">                                           
                          </div>
                      </div>                        
                    <hr>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">{{ $partido->equipo_B }} 
                                    <img src="/img/{{ $partido->flat_B }}.png">
                            </label>
                            <input type="hidden" class="form-control" name="equipo_B"  value=" {{ $partido->equipo_B }} ">
                            <div class="col-sm-8">
                                <small>Cuántos goles hace</small>
                                <input type="text" class="form-control" name="goles_B"  value=" {{ $partido->goles_B }} " autocomplete="off"> 
                                {!! $errors->first('goles_B', '<span class=error>:message</span>') !!}
                                <small>Introduce los minutos en los que se podría dar cada gol, sepáralos por coma(,) si es cero deja en blanco</small>
                                <input type="text" class="form-control" name="minGolesB"   value= " {{ $partido->minGolesB }}" placeholder="ejemplo 5,27,90"  autocomplete="off"> 
                                                      
                            </div>
                        </div>             

                    </div>
                    
                    <div class="box-footer">
                                      
                        <button type="submit" class="btn btn-info pull-right">Guardar</button>
                    </div>
                
                </form>
                 
                  
            </div>
        </div>
        {{-- <div class="col-sm-6">
             <div class="box box-info ">
                <div class="box-header with-border">
                    <h3 class="box-title"> Partidos del grupo A</h3>
                </div>
            
            <div class="box-body">
                <div class="btn-group">
                    <a type="button" class="btn btn-info">1</a>
                    <a type="button" class="btn btn-info">2</a>
                    <a type="button" class="btn btn-info">3</a>
                    <a type="button" class="btn btn-info">4</a>
                    <a type="button" class="btn btn-info">5</a>
                    <a type="button" class="btn btn-info">6</a>
                </div>
            </div>
            <div class="box-footer">
            </div>
            </div>
       
        </div> --}}
@stop
 
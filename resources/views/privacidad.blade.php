@extends('layouts.app')
@section('content')
<div class="container">
  <h1>Quiénes somos</h1>
<p><strong>GolPro.es</strong>, es un sitio Web enfocado a la divulgación de noticias para ser usada de manera personal y no comercial por parte de los usuarios, por lo que el visitante no podrá copiar, modificar, distribuir, transmitir, desplegar, ejecutar, reproducir, publicar, licenciar o crear trabajos derivados del presente sitio sin la autorización previa o resguardando creditos de origen.</p>
<p>La dirección de nuestra web es: https://GolPro.es.</p>
<p><strong>Qué datos personales recogemos y por qué los recogemos</strong></p>
<p><strong>GolPro.es</strong> ofrece información a los usuarios a través de un portal informativo en los términos y condiciones aquí establecidos. Puede igualmente ofrecer servicios o foros en los cuales el usuario pueda participar emitiendo opiniones o interactuando con otros usuarios.</p>
<p>En consecuencia, al utilizar el portal se establece un vínculo legal entre el usuario y <strong>Golpro</strong> regido por las leyes aplicables y por los términos y condiciones legales vigentes al momento de cada uso del portal que estarán en todo momento disponibles en la URL http://GolPro.es</p>
<p>Este acuerdo puede cambiar en el tiempo por lo que es responsabilidad del usuario verificar los términos y condiciones vigentes al momento de cada visita al portal o uso de servicios y verificar los últimos cambios. A tales efectos, al inicio del documento siempre aparecerá la fecha de última modificación.</p>
<p>Estos términos y condiciones de servicio se aplicarán a cada una de las visitas al portal individualmente consideradas realizadas por el usuario.</p>
<p>Si el usuario, cualquiera sea la intención de uso, no están de acuerdo con los términos y condiciones de uso vigentes en algún determinado momento deberá abstenerse de acceder al portal.</p>
<p>El usuario asume la responsabilidad por el uso que le da al portal que opera Diario La Nación, responsabilidad que se extiende al uso adecuado, cívico, correcto y diligente de los contenidos y servicios que eventualmente pudieran ofrecerse tales como chats, foros o grupos de noticias.</p>
<p>El acceso al portal es libre para cualquier persona que desee informarse de los contenidos allí expuestos y el simple acceso constituye la manifestación de voluntad válida del usuario de utilizar el mismo en las condiciones vigentes al momento de su uso.</p>
<p>La información se ofrece de manera gratuita y no constituye ningún tipo de recomendación ni consejo alguno por parte de Diario La Nación. Cualquier decisión que el usuario deba tomar es bajo su propia responsabilidad y por tanto Golpro. como Diario La Nación recomienda asesorarse adecuadamente con profesionales especializados en cada materia.</p>
<p>El usuario reconoce y acepta que <strong>GolPro.es</strong> se reserva el derecho en cualquier momento de modificar o interrumpir el servicio (o cualquier parte del mismo) con o sin previo aviso, y que Panorama no será responsable ante el usuario ni ante ningún tercero por cualquier modificación, suspensión o interrupción del servicio en estos casos.</p>
<p><strong>Comentarios</strong></p>
<p>Por los momentos nuestra sección de comentarios está desactivada dentro de nuestro portal, así que no hacemos uso de recopilación de datos por esta vía.</p>
<p>Sin embargo, de volver a estar activa en nuestra sección de noticias, los vivitantes sabrán que <strong>GolPro.es</strong> recopilará los datos que se muestran en el formulario de comentarios, así como la dirección IP del visitante y la cadena de agentes de usuario del navegador para ayudar a la detección de spam.</p>
<p>Una cadena anónima creada a partir de tu dirección de correo electrónico (también llamada hash) puede ser proporcionada al servicio de Gravatar para ver si la estás usando. La política de privacidad del servicio Gravatar está disponible aquí: https://automattic.com/privacy/. Después de la aprobación de tu comentario, la imagen de tu perfil es visible para el público en el contexto de su comentario.</p>
<p><strong>Medios</strong><br />
<strong>GolPro.es</strong> no permite la subida de imágenes u otro tipo de archivos a sus usuarios, y menos de imágenes o archivos que puedan enviar datos de ubicación (GPS EXIF) incluidos. Sin embargo no están exceptos que los visitantes de la web pueden descargar y extraer cualquier dato de localización de las imágenes de la web.</p>
<p><strong>Formularios de contacto</strong></p>
<p><strong>Cookies</strong></p>
<p><strong>Diario La Nación</strong> o de ahora en adelante <strong>GolPro.es</strong> podrá emplear cookies para personalizar y facilitar al máximo la experiencia del usuario visitando el portal.</p>
<p>Un <strong>cookie</strong> es una pequeña cantidad de información enviada por el portal y almacenada en el programa navegador del usuario, de manera que al ser consultada la actividad previa del usuario en el portal se pueda mejorar su experiencia navegando por el mismo.</p>
<p>En el portal el usuario encontrará enlaces o vínculos a otros sitios web. <strong>GolPro.es</strong> hace sus mejores esfuerzos para asociarse con páginas de buena reputación, sin embargo, no garantiza ni se hace responsable por los contenidos presentados en las páginas que reciben esos enlaces, ni garantiza la licitud o veracidad de los contenidos presentados en tales páginas.</p>
<p><strong>Qué tipo de datos se guardan en Cookies</strong><br />
Si dejas un comentario en nuestro sitio puedes elegir guardar tu nombre, dirección de correo electrónico y web en cookies. Esto es para tu comodidad, para que no tengas que volver a rellenar tus datos cuando dejes otro comentario. Estas cookies tendrán una duración de un año.</p>
<p>Si tienes una cuenta y te conectas a este sitio, instalaremos una cookie temporal para determinar si tu navegador acepta cookies. Esta cookie no contiene datos personales y se elimina al cerrar el navegador.</p>
<p>Cuando inicias sesión, también instalaremos varias cookies para guardar tu información de inicio de sesión y tus opciones de visualización de pantalla. Las cookies de inicio de sesión duran dos días, y las cookies de opciones de pantalla duran un año. Si seleccionas &#8220;Recordarme&#8221;, tu inicio de sesión perdurará durante dos semanas. Si sales de tu cuenta, las cookies de inicio de sesión se eliminarán.</p>
<p><strong>Contenido incrustado de otros sitios web</strong><br />
Los artículos de este sitio pueden incluir contenido incrustado (por ejemplo, vídeos, imágenes, artículos, etc.). El contenido incrustado de otras web se comporta exactamente de la misma manera que si el visitante hubiera visitado la otra web.</p>
<p>Estas web pueden recopilar datos sobre ti, utilizar cookies, incrustar un seguimiento adicional de terceros, y supervisar tu interacción con ese contenido incrustado, incluido el seguimiento de su interacción con el contenido incrustado si tienes una cuenta y estás conectado a esa web.</p>
<p><strong>Analítica</strong></p>
<p><strong>Con quién compartimos tus datos</strong></p>
<p>Nosotros <strong>GolPro.es</strong> directamente <strong>no</strong> recopilamos datos, se usan herramientas de terceros, ampliamente conocidas como <strong>Analytics de Google</strong> que nos brindan información de la actividad que hacen nuestros usuarios en nuestro portal.</p>
<p>Otra herramienta que recopila datos es nuestro sistema de suscripción, con previa autorización del usuario, Solo se conserva la dirección del email. El usuario es libre de <strong>anular la suscripción</strong> por lo que sus datos de correo electrónico pueden ser borrados de nuestra base de datos.</p>
<p><strong>Cuánto tiempo conservamos tus datos</strong></p>
<p>Si dejas un comentario, el comentario y sus metadatos se conservan indefinidamente. Esto es para que podamos reconocer y aprobar comentarios sucesivos automáticamente en lugar de mantenerlos en una cola de moderación.</p>
<p>De los usuarios que se registran en nuestra web (si los hay), también almacenamos la información personal que proporcionan en su perfil de usuario. Todos los usuarios pueden ver, editar o eliminar su información personal en cualquier momento (excepto que no pueden cambiar su nombre de usuario). Los administradores de la web también pueden ver y editar esa información.</p>
<p><strong>Qué derechos tienes sobre tus datos</strong></p>
<p>Si tienes una cuenta o has dejado comentarios en esta web, puedes solicitar recibir un archivo de exportación de los datos personales que tenemos sobre ti, incluyendo cualquier dato que nos hayas proporcionado. También puedes solicitar que eliminemos cualquier dato personal que tengamos sobre ti. Esto no incluye ningún dato que estemos obligados a conservar con fines administrativos, legales o de seguridad.</p>
<p><strong>Dónde enviamos tus datos</strong></p>
<p>Los comentarios de los visitantes puede que los revise un servicio de detección automática de spam.</p>
<p><strong>Tu información de contacto</strong></p>
<p><strong>GolPro.es</strong> solicita expresamente al usuario que en caso de detectar cualquier violación de los términos y condiciones de uso lo notifique al correo coordinacionweb@GolPro.es. Cualquier omisión de <strong>GolPro.es</strong> con respecto a una violación por parte del usuario no deja sin efecto nuestro derecho de actuar con respecto a incumplimientos posteriores.</p>
<p>Si el usuario desea comunicarse por medios físicos deberá hacerlo a la siguiente dirección: Calle 4 entre carreras 6 y 7 La Concordia, Edificio Diario La Nación, San Cristóbal &#8211; Venezuela.</p>
<p><strong>Información adicional</strong></p>
<p><strong>Cómo protegemos tus datos</strong><br />
No se comparte con nadie y el usuario puede eliminarlos o evitar acceso siempre.</p>
<p><strong>De qué terceros recibimos datos</strong><br />
Ninguno</p>
<p><strong>Qué tipo de toma de decisiones automatizada y/o perfilado hacemos con los datos del usuario</strong><br />
Ninguno</p>
<p><strong>Requerimientos regulatorios de revelación de información del sector</strong><br />
No hay requerimientos.</p> 
</div>

@endsection

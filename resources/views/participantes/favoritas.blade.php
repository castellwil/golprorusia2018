@extends('layouts.master')
@section('content')
   
   <h1>Lista de quinielas favoritas de </h1>
    @if( session()->has('info') )
        <div class="alert alert-success">{{ session('info') }}</div> 
    @endif
    
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th colspan="1">Player No</th>
                <th colspan="1">Nombre</th>
                <th colspan="3">Puntos </th>              
            </tr>
        </thead>
        <tbody>
            @foreach($points as $point)
            <tr>
                <td>{{ $point->user->id }}</td>
                <td> {{ $point->user->nombre }} {{ $point->user->apellido }}</td>              
                <td >
                    <div class="btn bg-navy btn-lg btn-flat" title="puntos">{{$point->puntos}}</div>
               
                    <div class="btn-group">
                            <button type="button" class="btn btn-default">+</button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <div class="btn bg-purple btn-xs" title="Aciertos Totales">{{$point->aciertos_totales}}</div>
                                    <small>Aciertos Totales</small>
                                </li>
                                <li>
                                        <div class="btn bg-olive btn-xs" title="Aciertos Parciales">{{$point->aciertos_parciales}}</div>
                                        <small> Aciertos Parciales</small>
                                </li>
                                <li>
                                    <div class="btn bg-navy btn-xs" title="goles">{{$point->goles}}</div>
                                   <small> Goles</small>
                                </li>

                            </ul>
                          </div>

                          
                          
                          


                </td>              
                <td> 
                    <a href="resumen/{{ $point->id  }}" class="btn btn-success btn-xs btn-flat">Ver quiniela</a> 
                    <a href="resumen/{{ $point->id  }}" class="btn bg-purple btn-xs btn-flat">Agregar a mi lista</a>            
                </td>
               
            </tr>
           
            @endforeach
        </tbody>
    </table>

  


    
@endsection

@extends('layouts.master')
@section('content')
   
   <h1>Lista de Usuarios</h1>
    @if( session()->has('info') )
        <div class="alert alert-success">{{ session('info') }}</div> 
    @endif
    
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th >Player No</th>
                <th >Nombre</th>
                <th >opciones</th>                       
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td> {{ $user->nombre }} {{ $user->apellido }}</td>              
                          
                <td> 
                    <a href="resumen/{{ $user->id  }}" class="btn btn-success btn-xs btn-flat">Ver quiniela</a> 
                    {{-- <a href="resumen/{{ $user->id  }}" class="btn bg-purple btn-xs btn-flat">Agregar a mi lista</a>             --}}
                </td>
               
            </tr>
           
            @endforeach
        </tbody>
    </table>

  


    
@endsection

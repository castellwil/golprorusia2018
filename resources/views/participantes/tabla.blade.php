@extends('layouts.master')
@section('content')
   
   <h1>Tabla de Posiciones</h1>
    @if( session()->has('info') )
        <div class="alert alert-success">{{ session('info') }}</div> 
    @endif

   
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th colspan="1">No</th>
                <th colspan="1">Player No</th>
                <th colspan="1">Nombre</th>
                <th colspan="3">Puntos </th>              
            </tr>
        </thead>
        <tbody>

               
            @foreach($points as $point)
            <tr>
                <td>{{ $key++ }}</td>
                <td>{{ $point->user->id }}</td>
                <td> {{ $point->user->nombre }} {{ $point->user->apellido }}</td>              
                <td >
                    <div class="btn bg-navy btn-lg btn-flat" title="puntos">{{$point->puntos}}</div>
               
                    

                          
                          
                          


                </td>              
                <td> 
                    <a href="resumen/{{ $point->user_id  }}" class="btn btn-success btn-xs btn-flat">Ver quiniela</a> 
                    {{--  <a href="resumen/{{ $point->id  }}" class="btn bg-purple btn-xs btn-flat">Agregar a mi lista</a>              --}}
                </td>
               
            </tr>
           
            @endforeach
        </tbody>
    </table>
    
    <div class="callout callout-danger">
            <h4>!Notifiacción!</h4>

            <p>Al final de la fase de grupos se actualizará los puntos correspondientes a los goles acertados en los minutos pronosticados. - Para buscar a un usuario usa Control F</p>
    </div>



    
@endsection

@extends('layouts.master')
@section('content')
   
   <h1>¡BIENVENIDOS!</h1>
   <br>
   <p>Gracias por registrarse en nuestra quiniela GolPro de Diario La Nación, lee las reglas y al final de la página haz clic en "CREAR TU QUINIELA". </p>

<h3>REGLAS DE LA QUINIELA GOLPRO DE DIARIO LA NACIÓN 2018:</h3>
<br><br>

<h4>1. PUNTOS POR ACERTAR EL "GANA, EMPATA O PIERDE"</h4>

    <p>---1.1 El participante obtendrá tres (3) puntos por acertar si GANA, EMPATA o PIERDE; por ejemplo: si Usted da ganador a Rusia sobre Arabia Saudita no importando el marcador.</p>

    <p>---1.2 Usted recibirá un (1) punto adicional si además de acertar el GANA, EMPATA o PIERDE,  acierta la cantidad de goles del equipo que aparece en la casilla superior de la quiniela, es decir, siguiendo con el ejemplo del partido Rusia vs. Arabia Saudita, si Usted acertó que Rusia gana a Arabia Saudita suma tres (3) puntos, pero si además acertó la cantidad de goles de Rusia, suma otro punto, lo que le daría un total de cuatro (4) puntos.</p>

    <P>---1.3 Usted sumará otro punto si acierta el número de goles del equipo que aparece en la casilla inferior. 
            <b>
                    Es decir, que si Usted acierta el marcador perfecto estaría sumando cinco (5) puntos: Tres (3) por el GANA, EMPATA o PIERDE + Uno (1) por gol del equipo de la casilla superior + Uno (1) por gol del equipo de la casilla inferior. 
        
            </b></P>

            <br><br>
    <h4>2. PUNTOS POR ADIVINAR LOS MINUTOS DE LOS GOLES </h4>

    <p> Al llenar su predicción, Usted se encontrará con un campo de texto adicional en el formulario de cada partido, el cual es para colocar los minutos probables en los que Usted cree que se podrían dar los goles, tanto del equipo de la casilla superior como el de la casilla inferior. <b>POR CADA GOL ACERTADO RECIBIRÁ DOS PUNTOS ADICIONALES, así que:   </b>     <p>

    <p>--- 2.1 Si Usted predice que Rusia gana 3 - 1 a Arabia Saudita, entonces deberá colocar en el campo indicado tres cifras separadas por una coma (,) indicando los minutos en los que se anotarían esos goles; por ejemplo: 12,55,88.</p>

    <p><b>ATENCIÓN: </b> La cantidad de cifras a colocar en el campo de los minutos, tendrá que corresponderse con la cantidad de goles que Usted predice, por ejemplo: si colocó que Rusia gana por tres (3) goles, en el campo de minutos sólo deberán aparecer tres (3) cifras: 2,36,78. </p>

    <p>---2.2 Para acertar los goles en los minutos usaremos la regla del +1 y -1. Por ejemplo: si Usted pronostica que el gol de un equipo será marcado en el minuto 7, usted recibiría los puntos si el gol es marcado efectivamente en el minuto 7, o en el minuto 7 + 1, es decir en el 8, o en el minuto 7 - 1, es decir en el 6. </p>
    <br>
    <br>

    <h4>3. UNA HORA ANTES DE CADA PARTIDO SE CERRARÁ EL SISTEMA DE PRONÓSTICOS:  </h4>
    <p>Usted podrá llenar o editar sus pronósticos hasta una hora antes de cada partido, es decir, el sistema cerrará el partido 60 minutos antes de que comience a jugarse. </p>
    <br><br>
    <h4>4. CRITERIOS DE DESEMPATE: </h4>
    <p>LOS PUNTOS son el primer criterio de desempate entre los usuarios que participan en la <b>QUINIELA GOLPRO DE DIARIO LA NACIÓN 2018</b>, pero además se tomarán otros ítems de ser necesario, por ejemplo: cantidad de acierto totales, es decir, resultados perfectos;  goles y aciertos parciales.  </p>
    <p>En caso de persistir empate entre los ganadores de la QUINIELA GOLPRO DE DIARIO LA NACIÓN 2018, el premio deberá repartirse en PARTES IGUALES. </p><br><br>

    
        <b>PREMIACIÓN: </b><br><br>
         <p>1° Lugar: Campeón de LA QUINIELA GOLPRO DIARIO LA NACIÓN 2018: BsF 15 millones.
        <p>2° Lugar: Subcampeón de LA QUINIELA GOLPRO DIARIO LA NACIÓN 2018: BsF 10 millones.</p>
        <p>3° Lugar: BsF 5 millones.</p><br><br>
        <p><b>¡SUERTE!</b></p>

    </p>



    @if($mostrarBoton)
    <h3>Crea Tu Quiniela</h3>
        <a href="/config1968pro" class="btn btn-block btn-warning btn-lg">Crear Quiniela</a>      
    @endif

    {{-- Si tienes alguna duda o inquietud
    <a href="https://m.me/lanaciondiario">Envíanos un mensaje de Facebook</a> ó --}}
    {{--  <a href="https://api.whatsapp.com/send?phone=+58">Envíanos un mensaje de WhatsApp</a>  --}}
@endsection

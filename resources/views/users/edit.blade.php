@extends('layouts.master')
@section('content')
   
   
   @if(session()->has('info'))
    <div class="alert alert-success">{{ session('info') }}</div>
    @endif
    

    <div class="row">
        <div class="col-sm-6">
            <div class="box box-info">
                <div class="box-header with-border">
                        <h2>Editar usuario</h2>
                        <form method="POST" action="{{ route('usuarios.update', $user->id) }}">
                                {!! method_field('PUT') !!}
                                {!! csrf_field() !!}
                                    <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                                        <label for="nombre" class="col-md-4 control-label">Nombre</label>
                
                                            <div class="col-md-6">
                                                <input id="name" type="text" class="form-control" name="nombre" value="{{ $user->nombre }}" required autofocus>
                
                                                @if ($errors->has('nombre'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('nombre') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                    </div>
                
                                    <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                                        <label for="apellido" class="col-md-4 control-label">Apellido</label>
                
                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="apellido" value="{{ $user->apellido }}" required autofocus>
                
                                            @if ($errors->has('apellido'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('apellido') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                
                                    <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                        <label for="birthday" class="col-md-4 control-label">Fecha de Nacimiento</label>    
                                        <div class="col-md-6">
                                            <input id="birthday" type="date" class="form-control" name="birthday" value="{{ $user->birthday }}" required autofocus>
                
                                            @if ($errors->has('birthday'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('birthday') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                
                                    <div class="form-group{{ $errors->has('cedula') ? ' has-error' : '' }}">
                                        <label for="cedula" class="col-md-4 control-label">Cédula de identidad</label>
                
                                        <div class="col-md-6">
                                            <input id="name" type="number" class="form-control" name="cedula" value="{{ $user->cedula }}" required autofocus>
                
                                            @if ($errors->has('cedula'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('cedula') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                
                                    <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                                        <label for="telefono" class="col-md-4 control-label">telefono</label>
                
                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="telefono" value="{{ $user->telefono }}" required autofocus>
                
                                            @if ($errors->has('telefono'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('telefono') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">E-Mail</label>
                
                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required>
                
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                <hr>    
                                    <br>
                
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary btn-block">
                                                Actualizar
                                            </button>
                                        </div>
                                    </div>
                                </form> 
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{ auth()->user()->img }}" alt="User profile picture">
            
                    <h3 class="profile-username text-center">{{ auth()->user()->nombre }} {{ auth()->user()->apellido }}</h3>
    
                    <p class="text-muted text-center">Player {{ auth()->user()->id }}</p>
    
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Cambiar imagen</b> <a class="pull-right">1,322</a>
                        </li>                    
                    </ul>
                    </div>                       
                </div>
        </div>
    </div>

    
        
@endsection

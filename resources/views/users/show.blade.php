@extends('layouts.master')
@section('content')
<div class="row">   
    <div class="col-sm-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h1>{{ $user->name }}</h1>
                <table class="table table-bordered table-striped">       
                        <tr>               
                            <th>Nombre y apellido</th>
                            <td>{{ $user->nombre }} {{ $user->apellido }} </td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{ $user->email }}</td>
                        </tr> 
                        <tr>
                            <th>Roles</th>
                            <td>
                                @foreach($user->roles as $role)
                                {{ $role->display_name }}
                                @endforeach
                            </td>
                        </tr>               
                </table>                
            </div>
        </div>
    </div>
</div>
    

    @can('edit', $user)
    <a href="{{ route('usuarios.edit', $user->id) }}" class="btn btn-info">Editar</a>
    @endcan

 
@stop
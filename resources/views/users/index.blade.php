@extends('layouts.master')
@section('content')
   
   <h1>Lista de Usuarios</h1>
    @if( session()->has('info') )
        <div class="alert alert-success">{{ session('info') }}</div> 
    @endif
    
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Role</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td> <a href=" usuarios/{{ $user->id  }}" >{{ $user->nombre }}</a></td>
                <td>{{ $user->email }}</td>
                <td>
                    @foreach($user->roles as $role)
                        {{ $role->display_name }}
                   @endforeach
                </td>
                <td> 
                    <a href=" {{ route('usuarios.edit', $user->id) }}" class="btn btn-info btn-xs">Editar</a> 
                    <form style="display:inline" method="POST" action="{{ route('usuarios.destroy',  $user->id) }}">
                        <button type="submit" class="btn btn-danger btn-xs">Eliminar</button>
                        {!! csrf_field() !!}
                        {!! method_field('DELETE') !!}
                    </form>
                </td>
               
            </tr>
           
            @endforeach
        </tbody>
    </table>

  


    
@endsection

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
               
                color: #FFF;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 50px;
            }

            .links > a {
                color: #fff;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
   
   <p>Limitaciones y uso correcto de GOLPRO.ES.</p>
<p><strong>GOLPRO.ES</strong>, es un sitio Web enfocado a la divulgación de noticias para ser usada de manera personal y no comercial por parte de los usuarios, por lo que el visitante no podrá copiar, modificar, distribuir, transmitir, desplegar, ejecutar, reproducir, publicar, licenciar o crear trabajos derivados del presente sitio sin la autorización previa o resguardando creditos de origen.</p>
<p><strong>Editorial Torbes CA</strong>, y <strong>GOLPRO.ES</strong>, no garantizan de forma alguna el servicio ininterrumpido o libre de error, a través de esta sitio web, ni la exactitud y/o confiabilidad o contenido de la información presentada en esta página, y en tal sentido, el usuario acepta utilizar dicho servicio con las excepciones aquí descritas, y del mismo modo, <strong>GOLPRO.ES </strong>tampoco se hace responsable por daños y perjuicios causados por el retardo o interrupción en la transmisión del servicio.</p>
<p>Todo el contenido de esta página web, puede ser suministrado por terceros, por lo cual <strong>GOLPRO.ES</strong> no se hace responsable por la precisión o fidelidad de las mismas. Todas las declaraciones y conceptos emitidos en las entrevistas y artículos de opinión son de la exclusiva responsabilidad de sus autores. <strong>GOLPRO.ES</strong>, tampoco se hace responsable por la exactitud de horarios y lugares indicados en los avisos de esta página los cuales son de la exclusiva responsabilidad de sus proveedores.</p>
<p><strong>GOLPRO.ES</strong> declara que la información expuesta en esta página web es de su única y exclusiva propiedad, por lo que el usuario de esta página certifica que no hará uso total ni parcial del contenido de la misma para fines personales o distintos a los contenidos en el presente Aviso Legal.</p>
<p>Con respecto al uso correcto de <strong>GOLPRO.ES</strong>: Los usuarios se comprometen a la utilización de <strong>GOLPRO.ES</strong>, de conformidad con lo establecido en las leyes venezolanas, la moral, las buenas costumbres, los Tratados Internacionales y el orden público, aplicables en la República Bolivariana de Venezuela, y de ser el usuario de otro país, se hará conforme a las leyes del mismo, de acuerdo con lo dispuesto en el presente Aviso Legal, razón por la cual deberá abstenerse a la utilización de <strong>GOLPRO.ES</strong> con fines o efectos ilícitos, que puedan dañar o deteriorar a otro usuario o que creen perjuicios en contra de <strong>GOLPRO.ES</strong> o <strong>Editorial Torbes CA</strong> Así como tampoco, podrán utilizar ni la página web, ni la información contenida en ella para los siguientes fines:</p>
<p>Inducir, incite o promueva actuaciones delictivas, denigratorias, discriminatorias, difamatorias, infamantes, violentas o, en general, contrarias a la ley, a la moral y las buenas costumbres generalmente aceptadas o al orden público; Incitar a involucrarse en prácticas que origen peligro o que induzcan a una situación peligrosa, de riesgo o nocivas para la salud y el equilibrio mental.</p>
<p>Para violar los supuestos de protección de los derechos de propiedad intelectual o industrial pertenecientes a terceras personas, sin la previa autorización de los titulares de los mismos. Para influir en la violación de secretos empresariales de terceras personas o en el secreto de las comunicaciones. Para constituir casos de publicidad o competencia engañosa o desleal.</p>
<p><strong>Condiciones Legales &#8211; Derechos de Propiedad Intelectual y de Autor</strong>.<br />
Todo usuario de <strong>GOLPRO.ES</strong>, declara reconocer que la información contenida en esta página web, contiene información protegida por las leyes de propiedad intelectual, y las demás leyes aplicables, por lo que se encuentra conforme al contenido publicitario de los patrocinadores o anunciantes de <strong>Editorial Torbes CA</strong>, empresa a la que le corresponden todos los derechos y patentes del contenido encontrado en <strong>GOLPRO.ES</strong>.</p>
<p>Los derechos originales de autor sobre el diseño de esta página, han sido cedidos a <strong>Editorial Torbes CA</strong> El usuario de esta página debe presumir, y así lo acepta, que todas y cada una de las secciones, nombres, textos, frases, fotos, videos y grabaciones, están protegidos por derechos de autor y reproducción obtenidos por <strong>Editorial Torbes CA.</strong> Por lo tanto, el contenido de los mismos no podrá ser usado ni total ni parcialmente sin el consentimiento escrito dado por <strong>Editorial Torbes CA</strong></p>
<p><strong>Editorial Torbes CA</strong>, respeta los derechos de terceros, incluyendo los derechos de autor, y en tal sentido, exhorta a los terceros usuarios a hacer lo mismo. <strong>Editorial Torbes CA</strong>, podrá a su sola discreción, cancelar las cuentas o remover a los usuarios que violen los derechos de otros usuarios. Si el usuario considera que su trabajo ha sido copiado ilegalmente o que sus derechos han sido violados de alguna manera, deberá comunicarse con el administrador de <strong>GOLPRO.ES</strong> quien procesará el mencionado reclamo.</p>
<p><strong>Marca Comercial y Logotipos:</strong><br />
Todos los servicios que contengan el nombre de <strong>GOLPRO.ES</strong> y <strong>Editorial Torbes CA</strong>, pertenecen a la empresa, la cual se reserva todos los derechos que se establecen como marcas registradas por <strong>Editorial Torbes CA</strong>, incluyendo el logo de <strong>LANACIONWEB</strong>. Todas las marcas comerciales que aparecen en esta página Web, pertenecen a <strong>Editorial Torbes CA</strong>, y las mismas han sido debidamente registradas por la misma, o su uso le ha sido licenciado, quedando estrictamente prohibido el uso no autorizado por parte de terceros usuarios de <strong>GOLPRO.ES</strong> la cual se reserva el derecho de accionar los mecanismos legales que considere convenientes para hacer valer sus derechos tanto en Venezuela como en el Extranjero.</p>
<p>Delitos Informáticos: <strong>GOLPRO.ES</strong>, intentará a través de los mecanismos legales y respectivos, de mantener la integridad y la mayor estabilidad de esta página web, a efecto de dar cumplimiento a lo dispuesto en el presente Aviso Legal. <strong>Editorial Torbes CA</strong> no se hace responsable, y en este sentido el usuario lo acepta y la exonera, de cualesquiera daños puedan ocasionarse a un tercero, derivada de la manipulación ilegal o ilegítima de los sistemas informáticos de <strong>GOLPRO.ES</strong>, en menoscabo de la confianza y/o buena fé de los usuarios.</p>
<p><strong>Limitación de Responsabilidad por Virus</strong>:<br />
Los usuarios de esta página web entienden y aceptan que <strong>GOLPRO.ES</strong> no garantiza en forma alguna que los archivos de textos o gráficos disponibles para descarga del usuario, según lo previsto en estos avisos, estarán libres de virus, caballos de Troya u códigos de carácter contaminante y destructivo; el usuario es responsable en adoptar las medidas y chequeos suficientes para garantizar la exactitud de la información descargada de esta página, así como es responsable por el mantenimiento de cualquier medio reconstructivo de información, en caso de pérdida de la misma.</p>
<p><strong>Exoneración a Directivos:</strong><br />
El usuario declara que al ingresar a esta página web, mantendrá a cualquier trabajador o Directivo de <strong>Editorial Torbes CA</strong>, incluyendo Presidente, Directores, empleados y proveedores, libres de toda responsabilidad, daños y perjuicios incluyendo daño moral, lucro cesante o daño emergente.</p>
<p><strong>Exoneración General de Responsabilidad:</strong><br />
El usuario de esta página web libera a<strong> Editorial Torbes CA</strong>, de toda responsabilidad, por cualquier pérdida, daño, gasto o costo, incluyendo honorarios de abogados derivados de la violación de cualesquiera leyes relacionadas con esta página web y con las Condiciones Legales, Políticas de Privacidad y Términos de Uso de esta página. No Ejercicio de Derechos: La inactividad de parte de <strong>Editorial Torbes CA</strong>, en el ejercicio de cualesquiera de los derechos que le correspondan, no podrá interpretarse ni constituirá renuncia, ni parcial, ni total de las acciones legales a las cuales tiene derecho.</p>
<p>Acceso de menores de edad: En caso de que menores de edad accedan a <strong>GOLPRO.ES</strong>, se presume que lo han hecho con el debido consentimiento de sus representantes legales.</p>
<p>En el supuesto de violación de los derechos de propiedad intelectual:</p>
<p>Si algún usuario de <strong>GOLPRO.ES</strong>, llegara a considerar que cualquier contenido viole su derecho de propiedad intelectual, deberá notificar inmediatamente a Editorial Torbes CA, de ello, indicando con pruebas fehacientes del hecho que supone, e indicando todos los datos informativos de su persona a la dirección de contacto ubicada en la página web.</p>
<p><strong>Exclusión de garantías y de responsabilidad</strong>:<br />
<strong>Editorial Torbes CA</strong>, se excluye bajo todo lo dispuesto en el ordenamiento jurídico, cualquier responsabilidad por los daños y perjuicios de toda naturaleza que puedan deberse a cualquier acción y omisión que suscite con ocasión a la información dispuesta en <strong>GOLPRO.ES</strong>, así como tampoco garantiza la privacidad y seguridad de la utilización de <strong>GOLPRO.ES</strong>.</p>
<p><strong>Indemnización</strong>:<br />
En todos los casos mencionados y/o de aquellos que se deriven de éstos, el usuario acepta ser responsable y acuerda indemnizar, mantener y a salvo a <strong>Editorial Torbes CA</strong>, sus directores, funcionarios, empleados, colaboradores, proveedores, agentes y/o accionistas, en y de cualquier reclamo y/o demanda, incluyendo honorarios razonables de abogados y costas y gastos de juicios, hechos por terceras partes debido a, o derivado de: (a) el Contenido que entregue, publique, envíe por correo electrónico, por lo que <strong>GOLPRO.ES</strong>, se reserva el derecho de desconectar o remover al usuario de todas las comunidades, en cualquier momento y por cualquier momento y por cualquier razón, sin que esto implique o crea derecho de indemnización alguna para el usuario, quien reconoce y acepta que su participación en las comunidades es por su propio riesgo.</p>
<p>Terminación:<br />
<strong>GOLPRO.ES</strong> podrá, a su sola discreción, dar por terminado la prestación del servicio en cualquier momento y sin previo aviso dado al usuario, el cual acepta que <strong>GOLPRO.ES</strong> haciendo pleno uso de sus derechos, podrá sin notificación previa, terminar o suspender por cualquier causa o razón, el uso de la totalidad o parte de los servicios contenidos en esta página web, no siendo responsable frente a los usuarios o frente a terceros por dicha terminación o suspensión.</p>
<p><strong>Ley Aplicable:</strong><br />
Este contrato de página Web será interpretado en todas sus partes por las leyes venezolanas.</p>
<p><strong>Reclamaciones</strong>:<br />
Cualquier reclamo o acción que resulte de o que se relacione con el presente Aviso Legal o el uso de <strong>GOLPRO.ES</strong>, tendrá que ser presentada dentro de los dos meses inmediatos siguientes a la fecha en que el hecho del cual se derive el reclamo o a la acción legal correspondiente se haya verificado, o perderá ese derecho para siempre. Todo usuario manifiesta su aceptación a lo anterior, sin importar lo establecido o dispuesto en alguna legislación que señale lo contrario.</p>
<p>Modificaciones:<br />
<strong>GOLPRO.ES</strong>, se reserva la facultad de revisar en cualquier momento el presente Aviso Legal, por lo que todos los usuarios quedan obligados por dichas revisiones.</p>
<p><strong>Sociedades o Asociaciones:</strong><br />
Todo usuario reconoce y acepta que por ingresar a <strong>GOLPRO.ES</strong>, no se configura ningún tipo de sociedad, asociación, empresa mixta, agencia, mandato, ni acuerdo de ninguna otra clase con el propietario de esta página ya que se trata de un servicio de información de acuerdo a lo previsto en el presente Aviso Legal.</p>
<p><strong>Aceptación Términos y Condiciones</strong>:<br />
El usuario al acceder a los servicios de este sitio, declara que ha leído y aceptado de forma tácita y pena, los términos y condiciones de uso de <strong>GOLPRO.ES</strong>, de conformidad con el presente Aviso Legal y las leyes de la República Bolivariana de Venezuela.</p>

    </body>
</html>
